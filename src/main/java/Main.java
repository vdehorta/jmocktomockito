import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import model.TestMethod;
import service.*;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Convertit une classe de test JMock en Mockito
 * Pré-requis sur la structure du fichier de test JMock :
 * - aucune boucle dans les blocs de programmation des mocks d'appel de méthodes
 * - aucune boucle dans les blocs de vérification d'appel des mocks de mÃéthodes
 * Created by VDehorta on 14/04/2016.
 */
public class Main {

    public static void main(String[] args) {

        final String jMockFilePath = ".\\src\\main\\resources\\jmock\\BroadCastRPCImplTest.java";

        FileInputStream in = null;

        try {
            in = new FileInputStream(jMockFilePath);
            final CodeExtractorService codeExtractorService = ServiceFactory.getCodeExtractorService();
            final ParserService parserService = ServiceFactory.getParserService();
            final CodeCheckerService codeCheckerService = ServiceFactory.getCodeCheckerService();
            final ReformatService reformatService = ServiceFactory.getReformatService();
            final CompilationUnit jMockCompilationUnit = JavaParser.parse(in);

            reformatService.fromMultipleLineCommentsToBlockComment(jMockCompilationUnit); //Reformat comments

            final List<TestMethod> testMethods = codeExtractorService.getStreamOnTypedNodes(jMockCompilationUnit, MethodDeclaration.class, codeCheckerService::isValidJMockMethod)
                    .map(method -> parserService.parseTestMethod(method))
                    .collect(Collectors.toList());

            System.out.println(
                    testMethods.stream()
                            .map(m -> m.toString())
                            .collect(Collectors.joining("\n", testMethods.size() + " method(s) parsed :\n", "")));

            final TranslatorService translatorService = ServiceFactory.getTranslatorService();

            PrintStream resultPrintStream =new PrintStream(new FileOutputStream(".\\src\\main\\resources\\output\\BroadCastRPCImplTest.java"));

            resultPrintStream.println(
                    testMethods.stream()
                            .map(jMockMethod -> translatorService.translate(jMockMethod))
                            .map(jMockMethod -> jMockMethod.toString())
                            .collect(Collectors.joining("\n", testMethods.size() + " translated method(s) :\n", "")));



        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
    }
}
