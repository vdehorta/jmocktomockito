package exception;

/**
 * Created by vdehorta on 18/04/2017.
 */
public class UnexistingTranslationException extends RuntimeException {

    public UnexistingTranslationException(String detail) {
        super("Unexisting translation for that object : " + detail);
    }
}
