package model;

import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;

import java.util.List;

import static model.MethodCallResultType.VOID;

public class BehaviourSimulation {

    private List<Comment> comments;

    private MethodCallExpr method; //.getAccount("test", "pass")

    private MethodCallResultType resultType; //Void, Value, Exception

    private Expression result; //ex : "mockResult", new RuntimeException(), etc.

    public BehaviourSimulation(List<Comment> comments, MethodCallExpr method, MethodCallResultType resultType, Expression result) {
        this.comments = comments;
        this.method = method;
        this.resultType = resultType;
        this.result = result;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public MethodCallExpr getMethod() {
        return method;
    }

    public MethodCallResultType getResultType() {
        return resultType;
    }

    public Expression getResult() {
        return result;
    }


    public static final class BehaviourSimulationBuilder {
        private List<Comment> comments;
        private MethodCallExpr method; //.getAccount("test", "pass")
        private MethodCallResultType resultType; //Void, Value, Exception
        private Expression result; //ex : "mockResult", new RuntimeException(), etc.

        public BehaviourSimulationBuilder withComments(List<Comment> comments) {
            this.comments = comments;
            return this;
        }

        public BehaviourSimulationBuilder withMethod(MethodCallExpr method) {
            this.method = method;
            return this;
        }

        public BehaviourSimulationBuilder withResultType(MethodCallResultType resultType) {
            if (VOID.equals(resultType)) {
                this.result = null;
            }
            this.resultType = resultType;
            return this;
        }

        public BehaviourSimulationBuilder withResult(Expression result) {
            this.result = result;
            return this;
        }

        public BehaviourSimulation build() {
            return new BehaviourSimulation(comments, method, resultType, result);
        }
    }
}
