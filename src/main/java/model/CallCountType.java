package model;

/**
 * Created by VDehorta on 16/05/2016.
 */
public enum CallCountType {

    ALLOWING("allowing"),
    AT_LEAST("atLeast"),
    AT_MOST("atMost"),
    BETWEEN("between"),
    EXACTLY("exactly"),
    IGNORING("ignoring"),
    NEVER("never"),
    ONE("one"),
    ONE_OF("oneOf");


    private final String pattern;

    CallCountType(String pattern) {
        this.pattern = pattern;
    }

    public static CallCountType fromPattern(final String pattern) {
        for (CallCountType callCountType : values()) {
            if (callCountType.getPattern().equals(pattern)) {
                return callCountType;
            }
        }
        return null;
    }

    public String getPattern() {
        return pattern;
    }
}
