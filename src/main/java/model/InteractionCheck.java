package model;

import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.expr.MethodCallExpr;

import java.util.List;

public class InteractionCheck {

    private List<Comment> comments;

    private MethodCallExpr method; //.getAccount("test", "pass")

    private MethodCallCount callCount; //How many times was mock invoked

    private InteractionCheck(List<Comment> comments , MethodCallExpr method, MethodCallCount callCount) {
        this.comments = comments;
        this.method = method;
        this.callCount = callCount;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public MethodCallExpr getMethod() {
        return method;
    }

    public MethodCallCount getCallCount() {
        return callCount;
    }


    public static final class InteractionCheckBuilder {
        private List<Comment> comments;
        private MethodCallExpr method; //.getAccount("test", "pass")
        private MethodCallCount callCount; //How many times was mock invoked

        public InteractionCheckBuilder withComments(List<Comment> comments) {
            this.comments = comments;
            return this;
        }

        public InteractionCheckBuilder withMethod(MethodCallExpr method) {
            this.method = method;
            return this;
        }

        public InteractionCheckBuilder withCallCount(MethodCallCount callCount) {
            this.callCount = callCount;
            return this;
        }

        public InteractionCheck build() {
            return new InteractionCheck(comments, method, callCount);
        }
    }
}
