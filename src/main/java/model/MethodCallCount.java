package model;

import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.IntegerLiteralExpr;

import static model.CallCountType.IGNORING;
import static model.CallCountType.ONE;

/**
 * Created by VDehorta on 16/05/2016.
 */
public class MethodCallCount {

    private CallCountType callCountType;

    private Boolean hasCardinality1;
    private Expression cardinality1;

    private Boolean hasCardinality2;
    private Expression cardinality2; //Used for between cardinality

    private MethodCallCount(CallCountType callCountType, boolean hasCardinality1, Expression cardinality1, boolean hasCardinality2, Expression cardinality2) {
        this.callCountType = callCountType;
        this.hasCardinality1 = hasCardinality1;
        this.cardinality1 = cardinality1;
        this.hasCardinality2 = hasCardinality2;
        this.cardinality2 = cardinality2;
    }

    public CallCountType getCallCountType() {
        return callCountType;
    }

    public Boolean hasCardinality1() {
        return hasCardinality1;
    }

    public Expression getCardinality1() {
        return cardinality1;
    }

    public Boolean hasCardinality2() {
        return hasCardinality2;
    }

    public Expression getCardinality2() {
        return cardinality2;
    }

    @Override
    public String toString() {
        return "MethodCallCount{" +
                "callCountType=" + callCountType +
                ", hasCardinality1=" + hasCardinality1 +
                ", cardinality1=" + cardinality1 +
                ", hasCardinality2=" + hasCardinality2 +
                ", cardinality2=" + cardinality2 +
                '}';
    }

    public static final class MethodCallCountBuilder {
        private CallCountType callCountType;
        private Boolean hasCardinality1;
        private Expression cardinality1;
        private Boolean hasCardinality2;
        private Expression cardinality2; //Used for between cardinality

        public MethodCallCountBuilder withCallCountType(CallCountType callCountType) {
            switch (callCountType) {
                case ONE:
                case ONE_OF:
                    this.callCountType = ONE;
                    cardinality1 = new IntegerLiteralExpr(1);
                    hasCardinality1 = true;
                    hasCardinality2 = false;
                    break;
                case AT_LEAST:
                case AT_MOST:
                case EXACTLY:
                    this.callCountType = callCountType;
                    hasCardinality2 = false;
                    break;
                case BETWEEN:
                    this.callCountType = callCountType;
                    break;
                case IGNORING:
                case ALLOWING:
                    this.callCountType = IGNORING;
                    hasCardinality1 = false;
                    hasCardinality2 = false;
                    break;
                case NEVER:
                    this.callCountType = callCountType;
                    hasCardinality1 = false;
                    hasCardinality2 = false;
                    break;
                default:
                    throw new IllegalArgumentException("CallCountType " + callCountType + " not yet handled in MethodCallCount.MethodCallCountBuilder.withCallCountType !!!");
            }
            return this;
        }

        public MethodCallCountBuilder withCardinality1(Expression cardinality1) {
            this.cardinality1 = cardinality1;
            hasCardinality1 = true;
            return this;
        }

        public MethodCallCountBuilder withCardinality2(Expression cardinality2) {
            this.cardinality2 = cardinality2;
            hasCardinality2 = true;
            return this;
        }

        public MethodCallCount build() {
            return new MethodCallCount(callCountType, hasCardinality1, cardinality1, hasCardinality2, cardinality2);
        }
    }
}
