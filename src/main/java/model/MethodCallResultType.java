package model;

/**
 * Created by VDehorta on 13/05/2016.
 */
public enum MethodCallResultType {

    VALUE,
    EXCEPTION,
    VOID
}
