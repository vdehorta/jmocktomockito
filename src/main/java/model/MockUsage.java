package model;

import com.github.javaparser.ast.expr.NameExpr;

public class MockUsage {

    private NameExpr mockName; //accountDaoMock

    private BehaviourSimulation behaviourSimulation;

    private InteractionCheck interactionCheck;

    public MockUsage(NameExpr mockName, BehaviourSimulation behaviourSimulation, InteractionCheck interactionCheck) {
        this.mockName = mockName;
        this.behaviourSimulation = behaviourSimulation;
        this.interactionCheck = interactionCheck;
    }

    public BehaviourSimulation getBehaviourSimulation() {
        return behaviourSimulation;
    }

    public InteractionCheck getInteractionCheck() {
        return interactionCheck;
    }

    public NameExpr getMockName() {
        return mockName;
    }


    public static final class MockUsageBuilder {
        private NameExpr mockName; //accountDaoMock
        private BehaviourSimulation behaviourSimulation;
        private InteractionCheck interactionCheck;

        public MockUsageBuilder withMockName(NameExpr mockName) {
            this.mockName = mockName;
            return this;
        }

        public MockUsageBuilder withBehaviourSimulation(BehaviourSimulation behaviourSimulation) {
            this.behaviourSimulation = behaviourSimulation;
            return this;
        }

        public MockUsageBuilder withInteractionCheck(InteractionCheck interactionCheck) {
            this.interactionCheck = interactionCheck;
            return this;
        }

        public MockUsage build() {
            return new MockUsage(mockName, behaviourSimulation, interactionCheck);
        }
    }
}
