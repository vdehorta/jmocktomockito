package model;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.Statement;

import java.util.List;
import java.util.Objects;

/**
 * Created by VDehorta on 09/05/2016.
 */
public class TestMethod {

    private MethodDeclaration methodWithoutTestCode;
    private List<MockUsage> mockUsages;
    private Statement testedMethodCallStmt;

    public TestMethod(MethodDeclaration methodWithoutTestCode, List<MockUsage> mockUsages, Statement testedMethodCallStmt) {
        this.methodWithoutTestCode = methodWithoutTestCode;
        this.mockUsages = mockUsages;
        this.testedMethodCallStmt = testedMethodCallStmt;
    }

    public MethodDeclaration getMethodWithoutTestCode() {
        return methodWithoutTestCode;
    }

    public List<MockUsage> getMockUsages() {
        return mockUsages;
    }

    public Statement getTestedMethodCallStmt() {
        return testedMethodCallStmt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestMethod that = (TestMethod) o;
        return Objects.equals(methodWithoutTestCode, that.methodWithoutTestCode) &&
                Objects.equals(mockUsages, that.mockUsages) &&
                Objects.equals(testedMethodCallStmt, that.testedMethodCallStmt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(methodWithoutTestCode, mockUsages, testedMethodCallStmt);
    }

    @Override
    public String toString() {
        return "TestMethod{" +
                "methodWithoutTestCode=" + methodWithoutTestCode +
                ", mockUsages=" + mockUsages +
                ", testedMethodCallStmt=" + testedMethodCallStmt +
                '}';
    }

    public static final class TestMethodBuilder {
        private MethodDeclaration methodWithoutTestCode;
        private List<MockUsage> mockUsages;
        private Statement testedMethodCall;

        public TestMethodBuilder withMethodWithoutTestCode(MethodDeclaration methodWithoutTestCode) {
            this.methodWithoutTestCode = methodWithoutTestCode;
            return this;
        }

        public TestMethodBuilder withMockUsages(List<MockUsage> mockUsages) {
            this.mockUsages = mockUsages;
            return this;
        }

        public TestMethodBuilder withTestedMethodCall(Statement testedMethodCall) {
            this.testedMethodCall = testedMethodCall;
            return this;
        }

        public TestMethod build() {
            return new TestMethod(methodWithoutTestCode, mockUsages, testedMethodCall);
        }
    }
}
