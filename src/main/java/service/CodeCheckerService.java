package service;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.Statement;

/**
 * Created by vdehorta on 28/03/2017.
 */
public interface CodeCheckerService {

    boolean isJMockExpectationStmt(final ExpressionStmt statement);

    boolean containsLoop(final MethodCallExpr methodCallMockBlock);

    boolean isJMockCallCountMethod(final MethodCallExpr methodCallExpr);

    boolean isSequence(final Statement statement);

    boolean isMockReturnStatement(final Statement statement);

    boolean isJMockAssertIsSatisfiedStmt(final Statement statement);

    boolean isValidJMockMethod(final MethodDeclaration method);

    boolean isJMockFirstStatement(ExpressionStmt statement);
}
