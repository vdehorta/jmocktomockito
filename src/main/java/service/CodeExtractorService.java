package service;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.Statement;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by vdehorta on 28/03/2017.
 */
public interface CodeExtractorService {

    <T extends Node> List<T> extractNodes(Node node, Class<T> type);

    <T extends Node> List<T> extractMatchingNodes(Node node, Class<T> type, Predicate<T> predicate);

    <T extends Node> Optional<T> extractFirstMatchingNode(Node node, Class<T> type, Predicate<T> predicate);

    <T extends Node> T extractFirstMatchingNodeOrNull(Node node, Class<T> type, Predicate<T> predicate);

    <T extends Node> Stream<T> getStreamOnTypedNodes(Node node, Class<T> type);

    <T extends Node> Stream<T> getStreamOnTypedNodes(Node node, Class<T> type, Predicate<T> predicate);

    <T extends Node> boolean anyMatchInNode(Node node, Class<T> type, Predicate<T> predicate);

    int findStmtIndexInMethod(Statement stmt, MethodDeclaration method);

    Optional<Statement> getNextStatement(Statement statement);
}
