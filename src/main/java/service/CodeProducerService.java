package service;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.Statement;

import java.util.List;

/**
 * Created by vdehorta on 28/03/2017.
 */
public interface CodeProducerService {

    MethodDeclaration insertStmtsIntoMethod(List<Statement> statements, MethodDeclaration method, int index);

    MethodDeclaration insertStmtsAtMethodEnd(List<Statement> statements, MethodDeclaration method);
}
