package service;

import com.github.javaparser.ast.body.MethodDeclaration;
import model.TestMethod;

/**
 * Created by vdehorta on 19/03/2017.
 */
public interface ParserService {

    /**
     * Try to convert the MethodDeclaration into a TestMethod object
     *
     * @param methodDeclaration
     * @return
     */
    TestMethod parseTestMethod(final MethodDeclaration methodDeclaration);
}
