package service;

import com.github.javaparser.ast.Node;

/**
 * Created by vdehorta on 19/03/2017.
 */
public interface ReformatService {


    /**
     * Reformat comments inside the given node
     * consecutive inline comments are grouped into block comments
     *
     * @param node
     */
    void fromMultipleLineCommentsToBlockComment(final Node node);
}
