package service;

import service.impl.*;

/**
 * Created by vdehorta on 19/03/2017.
 */
public class ServiceFactory {

    private static ReformatService reformatService;
    private static CodeCheckerService codeCheckerService;
    private static ParserService parserService;
    private static TranslatorService translatorService;
    private static CodeExtractorService codeExtractorService;
    private static CodeProducerService codeProducerService;

    public static ReformatService getReformatService() {
        if (reformatService == null) {
            reformatService = new ReformatServiceImpl();
        }
        return reformatService;
    }

    public static ParserService getParserService() {
        if (parserService == null) {
            parserService = new JMockParserServiceImpl();
        }
        return parserService;
    }

    public static CodeCheckerService getCodeCheckerService() {
        if (codeCheckerService == null) {
            codeCheckerService = new CodeCheckerServiceImpl();
        }
        return codeCheckerService;
    }

    public static TranslatorService getTranslatorService() {
        if (translatorService == null) {
            translatorService = new MockitoTranslatorServiceImpl();
        }
        return translatorService;
    }

    public static CodeExtractorService getCodeExtractorService() {
        if (codeExtractorService == null) {
            codeExtractorService = new CodeExtractorServiceImpl();
        }
        return codeExtractorService;
    }

    public static CodeProducerService getCodeProducerService() {
        if (codeProducerService == null) {
            codeProducerService = new CodeProducerServiceImpl();
        }
        return codeProducerService;
    }
}
