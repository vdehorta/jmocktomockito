package service;

import com.github.javaparser.ast.body.MethodDeclaration;
import model.TestMethod;

/**
 * Created by vdehorta on 15/04/2017.
 */
public interface TranslatorService {

    MethodDeclaration translate(final TestMethod testMethod);
}
