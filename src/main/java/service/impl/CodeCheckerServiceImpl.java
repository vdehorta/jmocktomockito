package service.impl;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import service.CodeCheckerService;
import service.CodeExtractorService;
import service.ServiceFactory;

import java.util.List;

import static model.CallCountType.*;

/**
 * Created by vdehorta on 28/03/2017.
 */
public class CodeCheckerServiceImpl implements CodeCheckerService {

    private static final SimpleName CHECKING_METHOD_NAME = new SimpleName("checking");
    private static final SimpleName EXPECTATIONS_CLASS_NAME = new SimpleName("Expectations");
    private static final SimpleName ASSERT_IS_SATISFIED_METHOD_NAME = new SimpleName("assertIsSatisfied");
    private static final SimpleName WILL_METHOD_NAME = new SimpleName("will");
    private static final SimpleName RETURN_VALUE_NAME = new SimpleName("returnValue");
    private static final SimpleName RETURN_ITERATOR_NAME = new SimpleName("returnIterator");
    private static final SimpleName THROW_EXCEPTION_NAME = new SimpleName("throwException");
    private static final SimpleName DO_ALL_NAME = new SimpleName("doAll");
    private static final SimpleName IN_SEQUENCE_NAME = new SimpleName("inSequence");

    private CodeExtractorService codeExtractorService = ServiceFactory.getCodeExtractorService();

    @Override
    public boolean isJMockExpectationStmt(final ExpressionStmt statement) {
        final List<MethodCallExpr> methodCalls = statement.getChildNodesByType(MethodCallExpr.class);

        if (methodCalls.isEmpty()) {
            return false;
        }

        final MethodCallExpr methodCallToCheck = methodCalls.get(0);

        if (containsLoop(methodCallToCheck)) {
            throw new RuntimeException("Can't parse code : presence of loop(s)");
        }

        return methodCallToCheck.getScope().isPresent()
                && methodCallToCheck.getName().equals(CHECKING_METHOD_NAME)
                && methodCallToCheck.getArguments().size() == 1
                && ObjectCreationExpr.class.equals(methodCallToCheck.getArgument(0).getClass())
                && ((ObjectCreationExpr) methodCallToCheck.getArgument(0)).getType().getName().equals(EXPECTATIONS_CLASS_NAME);
    }

    @Override
    public boolean containsLoop(final MethodCallExpr methodCallMockBlock) {
        return !methodCallMockBlock.getChildNodesByType(DoStmt.class).isEmpty()
                || !methodCallMockBlock.getChildNodesByType(ForeachStmt.class).isEmpty()
                || !methodCallMockBlock.getChildNodesByType(ForStmt.class).isEmpty()
                || !methodCallMockBlock.getChildNodesByType(WhileStmt.class).isEmpty();
    }

    @Override
    public boolean isJMockCallCountMethod(final MethodCallExpr methodCallExpr) {
        final String methodName = methodCallExpr.getName().getIdentifier();
        return ALLOWING.getPattern().equals(methodName)
                || AT_LEAST.getPattern().equals(methodName)
                || AT_MOST.getPattern().equals(methodName)
                || BETWEEN.getPattern().equals(methodName)
                || EXACTLY.getPattern().equals(methodName)
                || IGNORING.getPattern().equals(methodName)
                || NEVER.getPattern().equals(methodName)
                || ONE.getPattern().equals(methodName)
                || ONE_OF.getPattern().equals(methodName);
    }

    @Override
    public boolean isSequence(final Statement statement) {
        if (statement instanceof ExpressionStmt) {
            ExpressionStmt expressionStmt = (ExpressionStmt) statement;
            if (expressionStmt.getExpression() instanceof MethodCallExpr) {
                MethodCallExpr methodCallExpr = (MethodCallExpr) expressionStmt.getExpression();
                return IN_SEQUENCE_NAME.equals(methodCallExpr.getName());
            }
        }
        return false;
    }

    @Override
    public boolean isMockReturnStatement(final Statement statement) {
        if (statement instanceof ExpressionStmt) {
            ExpressionStmt expressionStmt = (ExpressionStmt) statement;

            //Check will method
            if (expressionStmt.getExpression() instanceof MethodCallExpr) {
                MethodCallExpr methodCallExpr = (MethodCallExpr) expressionStmt.getExpression();
                if (!WILL_METHOD_NAME.equals(methodCallExpr.getName())) {
                    return false;
                }

                NodeList<Expression> willMethodArguments = methodCallExpr.getArguments();
                if (willMethodArguments.size() != 1) {
                    return false;
                }

                //Check will argument method (returnValue(), returnIterator(), throwException(), doAll())
                Expression expression = willMethodArguments.get(0);
                if (!expression.getClass().equals(MethodCallExpr.class)) {
                    return false;
                }
                MethodCallExpr willArgumentMethod = (MethodCallExpr) expression;
                if (!RETURN_VALUE_NAME.equals(willArgumentMethod.getName())
                        && !RETURN_ITERATOR_NAME.equals(willArgumentMethod.getName())
                        && !THROW_EXCEPTION_NAME.equals(willArgumentMethod.getName())
                        && !DO_ALL_NAME.equals(willArgumentMethod.getName())) {
                    return false;
                }

                return !willArgumentMethod.getArguments().isEmpty();
            }
        }

        return false;
    }

    @Override
    public boolean isJMockAssertIsSatisfiedStmt(final Statement statement) {

        if (!statement.getClass().equals(ExpressionStmt.class)) {
            return false;
        }

        final Expression expression = ((ExpressionStmt) statement).getExpression();
        if (!expression.getClass().equals(MethodCallExpr.class)) {
            return false;
        }

        return ASSERT_IS_SATISFIED_METHOD_NAME.equals(((MethodCallExpr) expression).getName());
    }

    @Override
    public boolean isValidJMockMethod(final MethodDeclaration method) {
        return containsExpectationStmt(method);
    }

    @Override
    public boolean isJMockFirstStatement(final ExpressionStmt stmt) {
        return codeExtractorService.anyMatchInNode(stmt, MethodCallExpr.class, this::isJMockCallCountMethod);
    }

    private boolean containsExpectationStmt(final MethodDeclaration method) {
        return codeExtractorService.anyMatchInNode(method, ExpressionStmt.class, this::isJMockExpectationStmt);
    }
}
