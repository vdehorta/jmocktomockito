package service.impl;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.Statement;
import service.CodeExtractorService;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CodeExtractorServiceImpl implements CodeExtractorService {

    @Override
    public <T extends Node> List<T> extractNodes(Node node, Class<T> type) {
        return getStreamOnTypedNodes(node, type).collect(Collectors.toList());
    }

    @Override
    public <T extends Node> List<T> extractMatchingNodes(Node node, Class<T> type, Predicate<T> predicate) {
        return getStreamOnTypedNodes(node, type, predicate).collect(Collectors.toList());
    }

    @Override
    public <T extends Node> Optional<T> extractFirstMatchingNode(Node node, Class<T> type, Predicate<T> predicate) {
        return getStreamOnTypedNodes(node, type, predicate).findFirst();
    }

    @Override
    public <T extends Node> T extractFirstMatchingNodeOrNull(Node node, Class<T> type, Predicate<T> predicate) {
        return extractFirstMatchingNode(node, type, predicate).orElse(null);
    }

    @Override
    public <T extends Node> Stream<T> getStreamOnTypedNodes(Node node, Class<T> type) {
        return node.getChildNodesByType(type).stream();
    }

    @Override
    public <T extends Node> Stream<T> getStreamOnTypedNodes(Node node, Class<T> type, Predicate<T> predicate) {
        return getStreamOnTypedNodes(node, type).filter(predicate);
    }

    @Override
    public <T extends Node> boolean anyMatchInNode(Node node, Class<T> type, Predicate<T> predicate) {
        return getStreamOnTypedNodes(node, type).anyMatch(predicate);
    }

    @Override
    public int findStmtIndexInMethod(Statement stmt, MethodDeclaration method) {
        NodeList<Statement> methodStmts = method.getBody().get().getStatements();
        int statementIndex = -1;
        for (int i = 0; i < methodStmts.size(); i++) {
            if (methodStmts.get(i).equals(stmt)) {
                statementIndex = i + 1;
                break;
            }
        }
        return statementIndex;
    }

    @Override
    public Optional<Statement> getNextStatement(Statement statement) {
        if (statement.getParentNode().isPresent()) {
            final BlockStmt block = (BlockStmt) statement.getParentNode().get();
            final NodeList<Statement> blockStatements = block.getStatements();
            final int indexOfFirstStatementInBlock = blockStatements.indexOf(statement);
            if (indexOfFirstStatementInBlock <= blockStatements.size() - 2) {
                return Optional.of(blockStatements.get(indexOfFirstStatementInBlock + 1));
            }
        }
        return Optional.empty();
    }
}
