package service.impl;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.Statement;

import java.util.List;

public class CodeProducerServiceImpl implements service.CodeProducerService {

    @Override
    public MethodDeclaration insertStmtsIntoMethod(List<Statement> statements, MethodDeclaration method, int index) {
        MethodDeclaration resultMethod = method.clone();
        BlockStmt methodBody = resultMethod.getBody().get();
        int currentIndex = index;
        for (Statement stmt : statements) {
            methodBody.addStatement(currentIndex++, stmt);
        }
        return resultMethod;
    }

    @Override
    public MethodDeclaration insertStmtsAtMethodEnd(List<Statement> statements, MethodDeclaration method) {
        return insertStmtsIntoMethod(statements, method, method.getBody().get().getStatements().size());
    }
}
