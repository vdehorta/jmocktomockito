package service.impl;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.IntegerLiteralExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.Statement;
import model.*;
import model.BehaviourSimulation.BehaviourSimulationBuilder;
import model.MethodCallCount.MethodCallCountBuilder;
import model.MockUsage.MockUsageBuilder;
import service.CodeCheckerService;
import service.CodeExtractorService;
import service.ParserService;
import service.ServiceFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static model.CallCountType.*;
import static model.InteractionCheck.InteractionCheckBuilder;

/**
 * Created by vdehorta on 19/03/2017.
 */
public class JMockParserServiceImpl implements ParserService {

    private CodeCheckerService codeCheckerService = ServiceFactory.getCodeCheckerService();
    private CodeExtractorService codeExtractorService = ServiceFactory.getCodeExtractorService();

    @Override
    public TestMethod parseTestMethod(final MethodDeclaration testMethod) {
        return new TestMethod.TestMethodBuilder()
                .withMethodWithoutTestCode(extractMethodWithoutTestCode(testMethod))
                .withTestedMethodCall(parseTestedMethodCallStmt(testMethod))
                .withMockUsages(parseMockUsages(testMethod))
                .build();
    }

    protected MethodDeclaration extractMethodWithoutTestCode(final MethodDeclaration testMethod) {
        MethodDeclaration result = testMethod.clone();
        result.getBody().get().getStatements().removeIf(
                statement -> (statement instanceof ExpressionStmt &&
                        codeCheckerService.isJMockExpectationStmt((ExpressionStmt) statement))
                        || codeCheckerService.isJMockAssertIsSatisfiedStmt(statement)
        );
        return result;
    }

    protected List<MockUsage> parseMockUsages(final MethodDeclaration method) {
        final List<MockUsage> result = new ArrayList<>();
        final Statement expectationStmt = codeExtractorService.extractFirstMatchingNodeOrNull(method, ExpressionStmt.class, codeCheckerService::isJMockExpectationStmt);
        codeExtractorService.getStreamOnTypedNodes(expectationStmt, ExpressionStmt.class, codeCheckerService::isJMockFirstStatement)
                .forEach(firstStmt -> {
                    final Optional<Statement> maySecondStatement = codeExtractorService.getNextStatement(firstStmt);
                    if (!maySecondStatement.isPresent()
                            || (maySecondStatement.isPresent()
                            && !codeCheckerService.isJMockFirstStatement((ExpressionStmt) maySecondStatement.get()))
                            && !codeCheckerService.isSequence(maySecondStatement.get())) {
                        result.add(parseMockUsage(firstStmt, maySecondStatement));
                    }
                });
        return result;
    }

    private MockUsage parseMockUsage(final Statement firstStmt, final Optional<Statement> maySecondStmt) {
        final MethodCallExpr callCountMethod = codeExtractorService.extractFirstMatchingNodeOrNull(firstStmt, MethodCallExpr.class, codeCheckerService::isJMockCallCountMethod);
        final MethodCallExpr mockedMethod = parseMockCall(firstStmt);

        MethodCallResultType resultType = MethodCallResultType.VOID; //default value VOID
        Expression result = null;
        if (maySecondStmt.isPresent()) {
            final Statement secondStmt = maySecondStmt.get();
            resultType = parseMockResultType(secondStmt);
            result = parseMockResult(secondStmt);
        }

        final CallCountType callCountType = parseJMockCallCountType(callCountMethod);
        final NameExpr mockName = parseMockName(callCountMethod, callCountType);

        final BehaviourSimulation behaviourSimulation = new BehaviourSimulationBuilder()
                .withMethod(mockedMethod)
                .withComments(Collections.emptyList())
                .withResultType(resultType)
                .withResult(result)
                .build();

        final InteractionCheck interactionCheck = new InteractionCheckBuilder()
                .withMethod(mockedMethod)
                .withCallCount(parseMockCallCount(callCountMethod))
                .withComments(Collections.emptyList())
                .build();

        return new MockUsageBuilder()
                .withMockName(mockName)
                .withBehaviourSimulation(behaviourSimulation)
                .withInteractionCheck(interactionCheck)
                .build();
    }

    protected Statement parseTestedMethodCallStmt(final MethodDeclaration testMethod) {
        Statement expectationStmt = codeExtractorService.extractFirstMatchingNodeOrNull(testMethod, ExpressionStmt.class, codeCheckerService::isJMockExpectationStmt);
        return codeExtractorService.getNextStatement(expectationStmt).orElse(null);
    }

    protected CallCountType parseJMockCallCountType(final MethodCallExpr callCountMethod) {
        return CallCountType.fromPattern(callCountMethod.getName().getIdentifier());
    }

    protected MethodCallCount parseMockCallCount(final MethodCallExpr callCountMethod) {
        final String callCountMethodName = callCountMethod.getName().getIdentifier();
        final CallCountType callCountType = fromPattern(callCountMethodName);

        if (ONE.equals(callCountType) || ONE_OF.equals(callCountType)) {
            return new MethodCallCountBuilder()
                    .withCallCountType(EXACTLY)
                    .withCardinality1(new IntegerLiteralExpr(1))
                    .build();
        }

        if (AT_LEAST.equals(callCountType) || AT_MOST.equals(callCountType) || EXACTLY.equals(callCountType)) {
            return new MethodCallCountBuilder()
                    .withCallCountType(callCountType)
                    .withCardinality1(callCountMethod.getArguments().get(0))
                    .build();
        }
        if (BETWEEN.equals(callCountType)) {
            return new MethodCallCountBuilder()
                    .withCallCountType(callCountType)
                    .withCardinality1(callCountMethod.getArguments().get(0))
                    .withCardinality2(callCountMethod.getArguments().get(1))
                    .build();
        }
        return new MethodCallCountBuilder()
                .withCallCountType(callCountType)
                .build();
    }

    private Expression parseMockResult(final Statement resultStmt) {
        final MethodCallExpr resultTypeMethod = (MethodCallExpr) resultStmt.getChildNodes().get(0).getChildNodes()
                .get(1);
        return resultTypeMethod.getArguments().get(0);
    }

    private MethodCallResultType parseMockResultType(final Statement resultTypeStmt) {
        final MethodCallExpr resultTypeMethod = (MethodCallExpr) resultTypeStmt.getChildNodes().get(0).getChildNodes().get(1);
        final String mockResultTypeMethodName = resultTypeMethod.getName().getIdentifier();
        final MethodCallResultType methodCallResultType;
        switch (mockResultTypeMethodName) {
            case "returnValue":
                methodCallResultType = MethodCallResultType.VALUE;
                break;
            case "throwException":
                methodCallResultType = MethodCallResultType.EXCEPTION;
                break;
            default:
                throw new IllegalArgumentException("Cannot parse mock result type for JMock method '" + mockResultTypeMethodName + "' : not yet handled in ParserServiceImpl.parseMockResultType !!!");
        }
        return methodCallResultType;
    }

    /**
     * Parse the method call part of the statement
     *
     * @param statement
     * @return
     */
    protected MethodCallExpr parseMockCall(final Statement statement) {
        return (MethodCallExpr) ((ExpressionStmt) statement).getExpression();
    }

    protected NameExpr parseMockName(final MethodCallExpr methodCallExpr, final CallCountType callCountType) {
        if (needsJMockOfMethod(callCountType)) {
            return (NameExpr) ((MethodCallExpr) methodCallExpr.getParentNode().get()).getArgument(0);
        } else {
            return (NameExpr) methodCallExpr.getArgument(0);
        }
    }

    private boolean needsJMockOfMethod(CallCountType callCountType) {
        return EXACTLY.equals(callCountType) || AT_LEAST.equals(callCountType)
                || AT_MOST.equals(callCountType) || BETWEEN.equals(callCountType);
    }
}
