package service.impl;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.SimpleName;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.Statement;
import exception.UnexistingTranslationException;
import model.*;
import service.CodeExtractorService;
import service.CodeProducerService;
import service.ServiceFactory;
import service.TranslatorService;

import java.util.ArrayList;
import java.util.List;

import static model.MethodCallResultType.VOID;

/**
 * Created by vdehorta on 15/04/2017.
 */
public class MockitoTranslatorServiceImpl implements TranslatorService {

    private CodeExtractorService codeExtractorService = ServiceFactory.getCodeExtractorService();
    private CodeProducerService codeProducerService = ServiceFactory.getCodeProducerService();

    @Override
    public MethodDeclaration translate(final TestMethod testMethod) {
        List<MockUsage> mockUsages = testMethod.getMockUsages();
        List<Statement> whenThenStmts = new ArrayList<>();
        List<Statement> verifyStmts = new ArrayList<>();

        mockUsages.forEach(mockUsage -> {
            NameExpr mockName = mockUsage.getMockName();
            BehaviourSimulation behaviourSimulation = mockUsage.getBehaviourSimulation();
            if (needMockitoWhenThenStmt(behaviourSimulation)) {
                whenThenStmts.add(toWhenThenStmt(mockName, behaviourSimulation));
            }
            verifyStmts.add(toInteractionCheckStatement(mockName, mockUsage.getInteractionCheck()));
        });

        Statement testedMethodCallStmt = testMethod.getTestedMethodCallStmt();
        MethodDeclaration methodWithoutTestCode = testMethod.getMethodWithoutTestCode();
        return buildMockitoMethod(methodWithoutTestCode.clone(), testedMethodCallStmt.clone(), whenThenStmts, verifyStmts);
    }

    private MethodDeclaration buildMockitoMethod(MethodDeclaration methodWithoutTestCode, Statement testedMethodCallStmt, List<Statement> whenThenStmts, List<Statement> verifyStmts) {
        //when-then statements insertion
        int firstWhenThenStmtIndex = codeExtractorService.findStmtIndexInMethod(testedMethodCallStmt, methodWithoutTestCode) - 1;
        MethodDeclaration methodWithWhenThenStmts = codeProducerService.insertStmtsIntoMethod(whenThenStmts, methodWithoutTestCode, firstWhenThenStmtIndex);

        //verify statements insertion
        return codeProducerService.insertStmtsAtMethodEnd(verifyStmts, methodWithWhenThenStmts);
    }

    private boolean needMockitoWhenThenStmt(final BehaviourSimulation behaviourSimulation) {
        return VOID != behaviourSimulation.getResultType();
    }

    protected Statement toWhenThenStmt(final NameExpr mockName, final BehaviourSimulation behaviourSimulation) {
        final NameExpr mockito = new NameExpr("Mockito");
        final SimpleName when = new SimpleName("when");
        final MethodCallExpr mockCall = toBehaviourSimulationMethod(mockName, behaviourSimulation);
        final NodeList whenArguments = new NodeList<>();
        whenArguments.add(mockCall);
        final MethodCallExpr whenMethodCallExpr = new MethodCallExpr(mockito, when, whenArguments);
        final NodeList<Expression> thenArguments = new NodeList<>();
        thenArguments.add(behaviourSimulation.getResult());
        final MethodCallExpr whenThenMethodCallExpr = new MethodCallExpr(whenMethodCallExpr, new SimpleName("thenReturn"), thenArguments);
        return new ExpressionStmt(whenThenMethodCallExpr);
    }

    protected MethodCallExpr toBehaviourSimulationMethod(final NameExpr mockName, final BehaviourSimulation behaviourSimulation) {
        final MethodCallExpr translated = behaviourSimulation.getMethod().clone();
        translated.setScope(mockName.clone());
        return translated;
    }

    protected Statement toInteractionCheckStatement(NameExpr mockName, final InteractionCheck interactionCheck) {
        final NodeList<Expression> interactionCheckrguments = new NodeList<>();
        interactionCheckrguments.add(mockName.clone());
        interactionCheckrguments.add(toCallCountMethod(interactionCheck.getCallCount()));
        MethodCallExpr interactionCheckMethod = new MethodCallExpr(null, new SimpleName("verify"), interactionCheckrguments);
        final MethodCallExpr methodMockCall = interactionCheck.getMethod();
        MethodCallExpr mockVerifiedMethod = new MethodCallExpr(interactionCheckMethod, new SimpleName(methodMockCall.getName().getId()), new NodeList<>(methodMockCall.getArguments()));
        return new ExpressionStmt(mockVerifiedMethod);
    }

    protected MethodCallExpr toCallCountMethod(final MethodCallCount callCount) {
        SimpleName methodName = extractCallCountMethodName(callCount.getCallCountType());
        final NodeList<Expression> arguments = new NodeList<>();
        if (callCount.hasCardinality1()) {
            arguments.add(callCount.getCardinality1());

            if (callCount.hasCardinality2()) {
                arguments.add(callCount.getCardinality1());
            }
        }
        return new MethodCallExpr(null, methodName, arguments);
    }

    private SimpleName extractCallCountMethodName(CallCountType callCountType) {
        String methodName;
        switch (callCountType) {
            case NEVER:
                methodName = "never";
                break;
            case AT_LEAST:
                methodName = "atLeast";
                break;
            case AT_MOST:
                methodName = "atMost";
                break;
            case ONE:
            case EXACTLY:
                methodName = "times";
                break;
            case BETWEEN:
                throw new UnexistingTranslationException("Call count type = " + callCountType.toString());
            default:
                throw new IllegalArgumentException("CallCountType " + callCountType + " not yet handled in MockitoTranslatorServiceImpl.extractCallCountMethodName() !!!");
        }
        return new SimpleName(methodName);
    }
}
