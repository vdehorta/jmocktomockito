package service.impl;

import com.github.javaparser.Range;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.comments.LineComment;
import service.ReformatService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by vdehorta on 19/03/2017.
 */
public class ReformatServiceImpl implements ReformatService {

    @Override
    public void fromMultipleLineCommentsToBlockComment(final Node node) {
        final List<LineComment> allLineComments = node.getAllContainedComments().stream()
                .filter(comment -> comment instanceof LineComment)
                .map(lineComment -> (LineComment) lineComment)
                .collect(Collectors.toList());

        final List<LineComment> orphanLineComments = allLineComments.stream().filter(Comment::isOrphan).collect(Collectors.toList());

        //Generate mapping line number -> non-orphan
        final Map<Integer, LineComment> nonOrphanLineCommentByLine = allLineComments.stream()
                .filter(lineComment -> !lineComment.isOrphan())
                .collect(Collectors.toMap(lineComment -> lineComment.getBegin().get().line, lineComment -> lineComment));

        //If non-orphan coment is just after orphan comment line, replace the 2 consecutive line comment by a block comment
        orphanLineComments.stream().forEach(orphanLineComment -> {
            final int firstCommentLine = orphanLineComment.getBegin().get().line;
            final int nextLine = firstCommentLine + 1;
            if (nonOrphanLineCommentByLine.containsKey(nextLine)) {
                final LineComment nonOrphanLineComment = nonOrphanLineCommentByLine.get(nextLine);
                final BlockComment blockComment = buildBlockCommentFromLineComments(orphanLineComment, nonOrphanLineComment);
                orphanLineComment.remove();
                nonOrphanLineComment.getCommentedNode().get().setComment(blockComment);
            }
        });
    }

    private BlockComment buildBlockCommentFromLineComments(final LineComment firstLineComment, final LineComment secondLineComment) {
        final Range range = Range.range(firstLineComment.getBegin().get(), secondLineComment.getEnd().get());
        final String content = firstLineComment.getContent() + "\r\n" + secondLineComment.getContent();
        final BlockComment blockComment = new BlockComment(range, content);
        return blockComment;
    }
}
