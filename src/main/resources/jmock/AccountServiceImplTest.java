package m6.oat.service.ambassade;

import com.thoughtworks.xstream.core.util.Base64Encoder;
import junit.framework.TestCase;
import m6.oat.core.ambassade.Account;
import m6.oat.infra.dao.ambassade.AccountDao;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;

import java.security.MessageDigest;

/**
 * The Class AccountServiceImplTest.
 */
public class AccountServiceImplTest extends TestCase {

    /**
     * The mockery.
     */
    private Mockery mockery = new Mockery() {
        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };

    /**
     * The account dao mock.
     */
    private AccountDao accountDaoMock = mockery.mock(AccountDao.class);

    /**
     * The account service.
     */
    private AccountServiceImpl accountService;

    /**
     * @see junit.framework.TestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        accountService = new AccountServiceImpl();
        accountService.setAccountDao(accountDaoMock);
    }

    /**
     * Test get account.
     *
     * @throws Exception the exception
     */
    public void testGetAccount() throws Exception {
        mockery.checking(new Expectations() {
            {
                exactly(1).of(accountDaoMock).getAccount("test", "pass");
                will(returnValue(null));
            }
        });

        Account account = accountService.getAccount("test", "pass");
        assertNull(account);
        mockery.assertIsSatisfied();
    }

    /**
     * Test get account with clear pass.
     *
     * @throws Exception the exception
     */
    public void testGetAccountWithClearPass() throws Exception {
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA");
        mockery.checking(new Expectations() {
            {
                never(accountDaoMock).getAccount("test", new Base64Encoder().encode(messageDigest.digest("oat".getBytes())));
                will(returnValue(null));
            }
        });

        Account account = accountService.getAccountWithClearPass("test", "oat");
        assertNull(account);
        mockery.assertIsSatisfied();
    }
}
