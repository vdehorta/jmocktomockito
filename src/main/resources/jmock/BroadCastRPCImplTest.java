package m6.oat.ui.server.rpc.planning;

import m6.components.client.rpc.exception.BusinessRuleExceptionIS;
import m6.foundations.exception.BusinessRuleException;
import m6.oat.core.CoreHelper;
import m6.oat.core.planning.*;
import m6.oat.core.references.Channel;
import m6.oat.service.planning.BroadCastService;
import m6.oat.service.planning.results.AssociateBroadcastBreakResult;
import m6.oat.service.planning.results.ScheduleBreakExportResult;
import m6.oat.ui.client.models.core.ISHelper;
import m6.oat.ui.client.models.core.planning.*;
import m6.oat.ui.client.models.core.references.ChannelIS;
import m6.oat.ui.client.rpc.planning.results.AssociateBroadcastBreakResultIS;
import m6.oat.ui.client.rpc.planning.results.ScheduleBreakExportResultIS;
import m6.oat.ui.server.rpc.BaseRPCTest;
import net.sf.dozer.util.mapping.MapperIF;
import org.fest.assertions.Assertions;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BroadCastRPCImplTest extends BaseRPCTest {

    private BroadCastRPCImpl rpc;

    private Mockery mockery = new Mockery();
    // Defind mock
    final BroadCastService serviceMock = mockery.mock(BroadCastService.class);
    final MapperIF mapperMock = mockery.mock(MapperIF.class);
    final Sequence sequence = mockery.sequence("then");

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        rpc = new BroadCastRPCImpl();
        rpc.setBroadCastService(serviceMock);
        rpc.setMapper(mapperMock);
    }

    /**
     * Commentaire testFindMultiDiff
     *
     * @throws Exception
     */
    public void testFindMultiDiff() throws Exception {

        final ChannelIS channelIS = ISHelper.channelISW9();
        final Channel channel = CoreHelper.channelW9();
        channel.setId(channelIS.getId());

        final String programName = "NOMPROGRAMME";

        final DateTime beginDate = new DateTime(2008, 5, 1, 20, 30, 0, 0);
        final DateTime endDate = new DateTime(2008, 6, 1, 21, 40, 0, 0);

        final List<Broadcast> findAllTypes = new ArrayList<Broadcast>();
        findAllTypes.add(CoreHelper.broadcastSerie90());
        findAllTypes.add(CoreHelper.broadcastTeleReal());
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(channelIS, Channel.class);

                // BroadCastRPCImpl.mapper = mapper;
                will(returnValue(channel));


                // Define mock behaviour
                inSequence(sequence);
                one(serviceMock).findAll(channel, programName, beginDate, false, endDate, false);
                will(returnValue(findAllTypes));
                inSequence(sequence);

                exactly(findAllTypes.size()).of(mapperMock).map(with(any(Broadcast.class)), with(same(BroadcastIS.class)));
                //blablabla
                // tutu
                will(returnValue(ISHelper.broadcastSerieIS()));
            }
        });

        // Action
        List<BroadcastIS> ouput = rpc.findAll(channelIS, programName, beginDate.toLocalDate(), beginDate.getHourOfDay(), beginDate.getMinuteOfHour(),
                endDate.toLocalDate(), endDate.getHourOfDay(), endDate.getMinuteOfHour());

        // Test
        mockery.assertIsSatisfied();
        assertNotNull(ouput);
        assertEquals(2, ouput.size());
    }

    public void testFindAllBroadcastFromSPMI() throws Exception {
        // Setup
        final LocalDate diffusionTime = new LocalDate(2008, 12, 31);
        final List<Broadcast> broadcasts = new ArrayList<Broadcast>();
        broadcasts.add(CoreHelper.broadcastSerie90());
        broadcasts.add(CoreHelper.broadcastTeleReal());

        // Define mock behaviour
        mockery.checking();

        // Action
        List<BroadcastIS> ouput = rpc.findAllBroadcastFromSPMI(diffusionTime);

        // Test
        mockery.assertIsSatisfied();
        assertNotNull(ouput);
        assertEquals(2, ouput.size());
    }

    public void testFindAllBroadcastFromSPMINoDate() throws Exception {
        try {
            rpc.findAllBroadcastFromSPMI(null);
            fail();
        } catch (Exception e) {
            assertEquals(BusinessRuleExceptionIS.class, e.getClass());
        }

    }

    public void testCreateBroadcastOk() throws Exception {
        // Setup
        final BroadcastIS input = ISHelper.broadcastSerieIS();

        final Broadcast broadcast = CoreHelper.broadcastSerie90();
        broadcast.getChannel().setId(input.getChannel().getId());

        // Define mock behaviour
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Broadcast.class);
                will(returnValue(broadcast));

                inSequence(sequence);
                one(serviceMock).saveOrUpdate(broadcast);
                will(returnValue(broadcast));
                inSequence(sequence);

                one(mapperMock).map(broadcast, BroadcastIS.class);
                will(returnValue(input));

            }
        });
        // Action
        BroadcastIS output = rpc.saveOrUpdate(input);

        // Test
        assertNotNull(output);
        mockery.assertIsSatisfied();
    }

    public void testCreateKo() throws Exception {
        // Setup
        final BroadcastIS input = ISHelper.broadcastSerieIS();

        final Broadcast broadcast = CoreHelper.broadcastSerie90();
        broadcast.getChannel().setId(input.getChannel().getId());

        // Define mock behaviour
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Broadcast.class);
                will(returnValue(broadcast));

                inSequence(sequence);
                one(serviceMock).saveOrUpdate(broadcast);
                will(throwException(new BusinessRuleException("error test")));
            }
        });

        try {
            // Action
            rpc.saveOrUpdate(input);
            // Test
            fail();
        } catch (Exception e) {
            assertEquals(BusinessRuleExceptionIS.class.getName(), e.getClass().getName());
        }
        mockery.assertIsSatisfied();

    }

    public void testDeleteOk() throws Exception {
        // Setup
        final BroadcastIS input = ISHelper.broadcastSerieIS();

        final Broadcast broadcast = CoreHelper.broadcastSerie90();
        broadcast.setId(input.getId());

        // Define mock behaviour
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Broadcast.class);
                will(returnValue(broadcast));

                inSequence(sequence);
                one(serviceMock).delete(broadcast);
            }
        });

        // Action
        rpc.delete(input);

        // Test
        mockery.assertIsSatisfied();
    }

    public void testDeleteKo() throws Exception {
        // Setup
        final BroadcastIS input = ISHelper.broadcastSerieIS();

        final Broadcast broadcast = CoreHelper.broadcastSerie90();
        broadcast.getChannel().setId(input.getChannel().getId());

        // Define mock behaviour
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Broadcast.class);
                will(returnValue(broadcast));

                inSequence(sequence);
                one(serviceMock).delete(broadcast);
                will(throwException(new BusinessRuleException("error test")));
            }
        });

        try {
            // Action
            rpc.delete(input);
            // Test
            fail();
        } catch (Exception e) {
            assertEquals(BusinessRuleExceptionIS.class.getName(), e.getClass().getName());
        }
        mockery.assertIsSatisfied();

    }

    public void testGetScheduleBroadcastsOK() throws Exception {
        final ChannelIS channel = ISHelper.channelISM6();
        final Channel channelCore = CoreHelper.channelM6();
        final LocalDate beginDate = new LocalDate(2008, 10, 10);
        final LocalDate endDate = new LocalDate(2008, 10, 15);

        final Broadcast broadcast = CoreHelper.broadcastTeleReal();
        final ArrayList<Broadcast> list = new ArrayList<Broadcast>();
        list.add(broadcast);
        final BroadcastIS broadcastIS = ISHelper.broadcastTeleRealIS();

        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(channel, Channel.class);
                will(returnValue(channelCore));

                inSequence(sequence);
                one(serviceMock).getScheduleBroadcasts(channelCore, beginDate, endDate);
                will(returnValue(list));

                inSequence(sequence);
                one(mapperMock).map(broadcast, BroadcastIS.class);
                will(returnValue(broadcastIS));
            }
        });

        List<BroadcastIS> result = rpc.getScheduleBroadcasts(channel, beginDate, endDate);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(broadcastIS, result.get(0));
    }

    public void testGetScheduleBroadcastsKO() throws Exception {
        final ChannelIS channel = ISHelper.channelISM6();
        final Channel channelCore = CoreHelper.channelM6();
        final LocalDate beginDate = new LocalDate(2008, 10, 10);
        final LocalDate endDate = new LocalDate(2008, 10, 15);

        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(channel, Channel.class);
                will(returnValue(channelCore));

                inSequence(sequence);
                one(serviceMock).getScheduleBroadcasts(channelCore, beginDate, endDate);
                will(throwException(new BusinessRuleException("test")));

            }
        });

        try {
            rpc.getScheduleBroadcasts(channel, beginDate, endDate);
            fail();
        } catch (Exception e) {
            assertEquals(BusinessRuleExceptionIS.class.getName(), e.getClass().getName());
        }
        mockery.assertIsSatisfied();
    }

    public void testValidateScheduleBroadcastsOK() throws Exception {
        final ChannelIS channel = ISHelper.channelISM6();
        final Channel channelCore = CoreHelper.channelM6();
        final LocalDate beginDate = new LocalDate(2008, 10, 10);
        final LocalDate endDate = new LocalDate(2008, 10, 15);

        final Broadcast broadcast = CoreHelper.broadcastTeleReal();
        final ArrayList<Broadcast> list = new ArrayList<Broadcast>();
        list.add(broadcast);
        final BroadcastIS broadcastIS = ISHelper.broadcastTeleRealIS();
        final List<BroadcastIS> listIS = new ArrayList<BroadcastIS>();
        listIS.add(broadcastIS);

        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(channel, Channel.class);
                will(returnValue(channelCore));

                inSequence(sequence);
                one(mapperMock).map(broadcastIS, Broadcast.class);
                will(returnValue(broadcast));

                inSequence(sequence);
                one(serviceMock).validateScheduleImportList(channelCore, beginDate, endDate, list);
                will(returnValue(list));

                inSequence(sequence);
                one(mapperMock).map(broadcast, BroadcastIS.class);
                will(returnValue(broadcastIS));
            }
        });

        List<BroadcastIS> result = rpc.validateScheduleImportList(channel, beginDate, endDate, listIS);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(broadcastIS, result.get(0));
    }

    public void testValidateScheduleBroadcastsKO() throws Exception {
        final ChannelIS channel = ISHelper.channelISM6();
        final Channel channelCore = CoreHelper.channelM6();
        final LocalDate beginDate = new LocalDate(2008, 10, 10);
        final LocalDate endDate = new LocalDate(2008, 10, 15);

        final Broadcast broadcast = CoreHelper.broadcastTeleReal();
        final ArrayList<Broadcast> list = new ArrayList<Broadcast>();
        list.add(broadcast);
        final BroadcastIS broadcastIS = ISHelper.broadcastTeleRealIS();
        final List<BroadcastIS> listIS = new ArrayList<BroadcastIS>();
        listIS.add(broadcastIS);

        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(channel, Channel.class);
                will(returnValue(channelCore));

                inSequence(sequence);
                one(mapperMock).map(broadcastIS, Broadcast.class);
                will(returnValue(broadcast));

                inSequence(sequence);
                one(serviceMock).validateScheduleImportList(channelCore, beginDate, endDate, list);
                will(throwException(new BusinessRuleException("test")));

            }
        });

        try {
            rpc.validateScheduleImportList(channel, beginDate, endDate, listIS);
            fail();
        } catch (Exception e) {
            assertEquals(BusinessRuleExceptionIS.class.getName(), e.getClass().getName());
        }
        mockery.assertIsSatisfied();
    }

    public void testExportToSchedule() throws Exception {
        final Break breakCore = CoreHelper.break2012M6();
        final BreakIS<?> breakIS = ISHelper.breakIS_M6_2012();
        final List<Break> breaksCore = new ArrayList<Break>();
        breaksCore.add(breakCore);

        final Broadcast broadcastCore = CoreHelper.broadcastSerie90();
        final BroadcastIS broadcastIS = ISHelper.broadcastSerieIS();
        final List<Broadcast> broadcastsCore = new ArrayList<Broadcast>();
        broadcastsCore.add(broadcastCore);

        final List<BroadcastIS> broadcasts = new ArrayList<BroadcastIS>();
        broadcasts.add(broadcastIS);

        final ChannelIS channel = ISHelper.channelISM6();
        final Channel channelCore = CoreHelper.channelM6();

        final LocalDate beginDate = new LocalDate(2009, 1, 1);
        final LocalDate endDate = new LocalDate(2009, 1, 2);
        final List<BreakIS<?>> breaks = new ArrayList<BreakIS<?>>();
        breaks.add(breakIS);

        mockery.checking(new Expectations() {
            {
                exactly(1).of(mapperMock).map(channel, Channel.class);
                will(returnValue(channelCore));

                exactly(1).of(mapperMock).map(breakIS, Break.class);
                will(returnValue(breakCore));

                exactly(1).of(mapperMock).map(broadcastIS, Broadcast.class);
                will(returnValue(broadcastCore));

                exactly(1).of(serviceMock).exportToSchedule(channelCore, beginDate, endDate, broadcastsCore, breaksCore);
            }
        });

        rpc.exportToSchedule(channel, beginDate, endDate, broadcasts, breaks);
        mockery.assertIsSatisfied();
    }

    /**
     * Cas où export non fait pour cause breakError.
     *
     * @throws Exception
     */
    public void testExportToScheduleKoWithBreakErrors() throws Exception {
        final Break breakCore = CoreHelper.break2012M6();
        final BreakIS<?> breakIS = ISHelper.breakIS_M6_2012();
        final List<Break> breaksCore = new ArrayList<Break>();
        breaksCore.add(breakCore);

        final Broadcast broadcastCore = CoreHelper.broadcastSerie90();
        final BroadcastIS broadcastIS = ISHelper.broadcastSerieIS();
        final List<Broadcast> broadcastsCore = new ArrayList<Broadcast>();
        broadcastsCore.add(broadcastCore);

        final List<BroadcastIS> broadcasts = new ArrayList<BroadcastIS>();
        broadcasts.add(broadcastIS);

        final ChannelIS channel = ISHelper.channelISM6();
        final Channel channelCore = CoreHelper.channelM6();

        final LocalDate beginDate = new LocalDate(2009, 1, 1);
        final LocalDate endDate = new LocalDate(2009, 1, 2);
        final List<BreakIS<?>> breaks = new ArrayList<BreakIS<?>>();
        breaks.add(breakIS);

        final ScheduleBreakExportResult result = new ScheduleBreakExportResult();
        String errorMessage = "C'est n'importe quoi ce break";
        result.addBreakError(breakCore, errorMessage);

        final ScheduleBreakExportResultIS resultIS = new ScheduleBreakExportResultIS();


        mockery.checking(new Expectations() {
            {
                exactly(1).of(mapperMock).map(channel, Channel.class);
                will(returnValue(channelCore));

                exactly(1).of(mapperMock).map(breakIS, Break.class);
                will(returnValue(breakCore));

                exactly(1).of(mapperMock).map(broadcastIS, Broadcast.class);
                will(returnValue(broadcastCore));

                exactly(1).of(serviceMock).exportToSchedule(channelCore, beginDate, endDate, broadcastsCore, breaksCore);
                will(returnValue(result));

                exactly(1).of(mapperMock).map(result, ScheduleBreakExportResultIS.class);
                will(returnValue(resultIS));
            }
        });

        rpc.exportToSchedule(channel, beginDate, endDate, broadcasts, breaks);
        mockery.assertIsSatisfied();
    }


    public void testExportToScheduleKO() throws Exception {
        final Break breakCore = CoreHelper.break2012M6();
        final BreakIS<?> breakIS = ISHelper.breakIS_M6_2012();
        final List<Break> breaksCore = new ArrayList<Break>();
        breaksCore.add(breakCore);

        final Broadcast broadcastCore = CoreHelper.broadcastSerie90();
        final BroadcastIS broadcastIS = ISHelper.broadcastSerieIS();
        final List<Broadcast> broadcastsCore = new ArrayList<Broadcast>();
        broadcastsCore.add(broadcastCore);

        final List<BroadcastIS> broadcasts = new ArrayList<BroadcastIS>();
        broadcasts.add(broadcastIS);

        final ChannelIS channel = ISHelper.channelISM6();
        final Channel channelCore = CoreHelper.channelM6();

        final LocalDate beginDate = new LocalDate(2009, 1, 1);
        final LocalDate endDate = new LocalDate(2009, 1, 2);
        final List<BreakIS<?>> breaks = new ArrayList<BreakIS<?>>();
        breaks.add(breakIS);

        mockery.checking(new Expectations() {
            {
                exactly(1).of(mapperMock).map(channel, Channel.class);
                will(returnValue(channelCore));

                exactly(1).of(mapperMock).map(breakIS, Break.class);
                will(returnValue(breakCore));

                exactly(1).of(mapperMock).map(broadcastIS, Broadcast.class);
                will(returnValue(broadcastCore));

                exactly(1).of(serviceMock).exportToSchedule(channelCore, beginDate, endDate, broadcastsCore, breaksCore);
                will(throwException(new BusinessRuleException("Test")));
            }
        });

        try {
            rpc.exportToSchedule(channel, beginDate, endDate, broadcasts, breaks);
            fail();
        } catch (Exception e) {
            assertEquals(BusinessRuleExceptionIS.class.getName(), e.getClass().getName());
        }
        mockery.assertIsSatisfied();
    }

    public void testFindAllContext() {
        final ChannelIS channel = ISHelper.channelISM6();
        final Channel channelCore = CoreHelper.channelM6();

        final LocalDate beginDate = new LocalDate(2009, 1, 1);
        final LocalDate endDate = new LocalDate(2009, 1, 2);

        final List<BroadcastContext> result = new ArrayList<BroadcastContext>();
        result.add(CoreHelper.broadcastContextAntenne2012M6());
        result.add(CoreHelper.broadcastContextCommercial2012M6());

        mockery.checking(new Expectations() {
            {
                exactly(1).of(mapperMock).map(channel, Channel.class);
                will(returnValue(channelCore));

                exactly(1).of(serviceMock).findAllContext(channelCore, beginDate, endDate, false);
                will(returnValue(result));

                one(mapperMock).map(result.get(0).getTypeContext(), BroadcastContextTypeEnumIS.class);
                will(returnValue(BroadcastContextTypeEnumIS.ANTENNE));
                one(mapperMock).map(result.get(0), BroadcastContextAntenneIS.class);
                will(returnValue(ISHelper.broadcastContextAntenne2012M6()));

                one(mapperMock).map(result.get(1).getTypeContext(), BroadcastContextTypeEnumIS.class);
                will(returnValue(BroadcastContextTypeEnumIS.COMMERCIAL));
                one(mapperMock).map(result.get(1), BroadcastContextCommercialIS.class);
                will(returnValue(ISHelper.broadcastContextCommercial2012M6()));
            }
        });

        rpc.findAllContext(channel, beginDate, endDate, false);
        mockery.assertIsSatisfied();
    }

    public void testInit() throws Exception {
        rpc.init();
        assertTrue(true);// Maven
    }

    public void testAssociateBreakBroadCastOk() throws Exception {
        // Setup
        final BroadcastContextIS inputIS = ISHelper.broadcastContextCommercial2012M6();
        inputIS.setAfter(null);

        final List<BroadcastIS> broadcastsIS = new ArrayList<BroadcastIS>();
        broadcastsIS.add(inputIS.getBefore());

        final AssociateBroadcastBreakResultIS resultIS = new AssociateBroadcastBreakResultIS();
        resultIS.getUpdatedBroadcastContext().add(inputIS);

        final BroadcastContext inputCore = CoreHelper.broadcastContextCommercial2012M6();
        inputCore.setAfter(null);

        final List<Broadcast> broadcastsCore = new ArrayList<Broadcast>();
        broadcastsCore.add(inputCore.getBefore());

        final AssociateBroadcastBreakResult result = new AssociateBroadcastBreakResult();
        result.getUpdatedBroadcastContext().add(inputCore);

        // Define mock behaviour
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(broadcastsIS.get(0), Broadcast.class);
                will(returnValue(broadcastsCore.get(0)));

                one(mapperMock).map(inputIS.getBreak(), Break.class);
                will(returnValue(inputCore.getBreak()));

                one(mapperMock).map(inputIS.getBefore(), Broadcast.class);
                will(returnValue(inputCore.getBefore()));

                never(mapperMock).map(inputIS.getAfter(), Broadcast.class);

                one(mapperMock).map(BroadcastContextTypeEnumIS.COMMERCIAL, BroadcastContextTypeEnum.class);
                will(returnValue(BroadcastContextTypeEnum.COMMERCIAL));

                inSequence(sequence);
                one(serviceMock).associateBreakBroadCast(broadcastsCore, inputCore.getBreak(), inputCore.getBefore(), inputCore.getAfter(),
                        BroadcastContextCommercial.class);
                will(returnValue(result));
                inSequence(sequence);

                one(mapperMock).map(result, AssociateBroadcastBreakResultIS.class);
                will(returnValue(resultIS));
            }
        });
        // Action
        AssociateBroadcastBreakResultIS output = rpc.associateBreakBroadCast(broadcastsIS, inputIS.getBreak(), inputIS.getBefore(), inputIS.getAfter(),
                BroadcastContextTypeEnumIS.COMMERCIAL);

        // Test
        assertEquals(1, output.getUpdatedBroadcastContext().size());
        mockery.assertIsSatisfied();
    }

    public void testAssociateBreakBroadCastKo() throws Exception {
        // Setup
        final BroadcastContextIS inputIS = ISHelper.broadcastContextCommercial2012M6();

        final BroadcastContext inputCore = CoreHelper.broadcastContextCommercial2012M6();

        final List<BroadcastIS> broadcastsIS = new ArrayList<BroadcastIS>();
        broadcastsIS.add(inputIS.getBefore());

        final List<Broadcast> broadcastsCore = new ArrayList<Broadcast>();
        broadcastsCore.add(inputCore.getBefore());

        // Define mock behaviour
        mockery.checking(new Expectations() {
            {
                // Mapping des parametres
                one(mapperMock).map(broadcastsIS.get(0), Broadcast.class);
                will(returnValue(broadcastsCore.get(0)));

                one(mapperMock).map(inputIS.getBreak(), Break.class);
                will(returnValue(inputCore.getBreak()));

                one(mapperMock).map(inputIS.getBefore(), Broadcast.class);
                will(returnValue(inputCore.getBefore()));

                one(mapperMock).map(inputIS.getAfter(), Broadcast.class);
                will(returnValue(inputCore.getAfter()));

                one(mapperMock).map(BroadcastContextTypeEnumIS.COMMERCIAL, BroadcastContextTypeEnum.class);
                will(returnValue(BroadcastContextTypeEnum.COMMERCIAL));

                // Appel du service
                inSequence(sequence);
                one(serviceMock).associateBreakBroadCast(broadcastsCore, inputCore.getBreak(), inputCore.getBefore(), inputCore.getAfter(),
                        BroadcastContextCommercial.class);
                will(throwException(new BusinessRuleException("error test")));
            }
        });

        try {
            // Action
            rpc.associateBreakBroadCast(broadcastsIS, inputIS.getBreak(), inputIS.getBefore(), inputIS.getAfter(), BroadcastContextTypeEnumIS.COMMERCIAL);
            // Test
            fail();
        } catch (Exception e) {
            assertEquals(BusinessRuleExceptionIS.class.getName(), e.getClass().getName());
        }
        mockery.assertIsSatisfied();

    }

    public void testAssociateBreakBroadCastWithAffectOk() throws Exception {

        // Setup
        final BroadcastIS broadcastIS = ISHelper.broadcastSerieIS();
        final Broadcast broadcastCore = CoreHelper.broadcastSerie90();

        final BreakIS<?> breakIs = ISHelper.breakIS_M6_2012();
        final Break breakCore = CoreHelper.break0301FunTV();

        final List<BroadcastIS> broadcastsISList = new ArrayList<BroadcastIS>();
        broadcastsISList.add(broadcastIS);
        final List<BreakIS<?>> breaksISList = new ArrayList<BreakIS<?>>();
        breaksISList.add(breakIs);
        final List<Broadcast> broadcastsList = new ArrayList<Broadcast>();
        broadcastsList.add(broadcastCore);
        final List<Break> breaksList = new ArrayList<Break>();
        breaksList.add(breakCore);

        final AssociateBroadcastBreakResultIS result = new AssociateBroadcastBreakResultIS();
        result.addSuccess(BroadcastContextTypeEnumIS.COMMERCIAL, breakIs);
        result.addSuccess(BroadcastContextTypeEnumIS.ANTENNE, breakIs);

        final AssociateBroadcastBreakResult resultCore = new AssociateBroadcastBreakResult();
        resultCore.addSuccess(BroadcastContextTypeEnum.COMMERCIAL, breakCore);
        resultCore.addSuccess(BroadcastContextTypeEnum.ANTENNE, breakCore);

        mockery.checking(new Expectations() {
            {
                // Mapping des parametres
                one(mapperMock).map(broadcastIS, Broadcast.class);
                will(returnValue(broadcastCore));

                one(mapperMock).map(breakIs, Break.class);
                will(returnValue(breakCore));

                // Appel de la fonction du service
                one(serviceMock).associateBroadCastBreak(broadcastsList, breaksList, true);
                will(returnValue(resultCore));

                // Mapping du résultat
                one(mapperMock).map(resultCore, AssociateBroadcastBreakResultIS.class);
                will(returnValue(result));
            }
        });

        // Action
        AssociateBroadcastBreakResultIS resultList = rpc.associateBroadCastBreak(broadcastsISList, breaksISList, true);

        // Tests
        mockery.assertIsSatisfied();
        assertEquals(breakIs, resultList.getBreakResults().get(BroadcastContextTypeEnumIS.COMMERCIAL).getSuccess().get(0));
        assertEquals(breakIs, resultList.getBreakResults().get(BroadcastContextTypeEnumIS.ANTENNE).getSuccess().get(0));
    }

    public void testFindBroadCastContextFromBreakOk() throws Exception {
        // Setup
        final BreakIS<?> input = ISHelper.breakIS_M6_2012();
        final Break breakCore = CoreHelper.break2012M6();
        final BroadcastContextIS contextIS = ISHelper.broadcastContextCommercial2012M6();
        final BroadcastContext contextCore = CoreHelper.broadcastContextCommercial2012M6();
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Break.class);
                will(returnValue(breakCore));

                one(mapperMock).map(BroadcastContextTypeEnumIS.COMMERCIAL, BroadcastContextTypeEnum.class);
                will(returnValue(BroadcastContextTypeEnum.COMMERCIAL));

                one(serviceMock).findBroadcastFromBreak(breakCore, BroadcastContextCommercial.class);
                will(returnValue(contextCore));

                one(mapperMock).map(contextCore, BroadcastContextCommercialIS.class);
                will(returnValue(contextIS));

            }
        });
        // Action
        BroadcastContextIS output = rpc.findBroadcastContextFromBreak(input, BroadcastContextTypeEnumIS.COMMERCIAL);
        // Tests
        mockery.assertIsSatisfied();
        assertNotNull(output);
        assertEquals(contextIS, output);
    }

    public void testFindBroadCastContextFromBreakNotFound() throws Exception {
        // Setup
        final BreakIS<?> input = ISHelper.breakIS_M6_2012();
        final Break breakCore = CoreHelper.break2012M6();
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Break.class);
                will(returnValue(breakCore));

                one(mapperMock).map(BroadcastContextTypeEnumIS.COMMERCIAL, BroadcastContextTypeEnum.class);
                will(returnValue(BroadcastContextTypeEnum.COMMERCIAL));

                one(serviceMock).findBroadcastFromBreak(breakCore, BroadcastContextCommercial.class);
                will(returnValue(null));
            }
        });
        // Action
        BroadcastContextIS output = rpc.findBroadcastContextFromBreak(input, BroadcastContextTypeEnumIS.COMMERCIAL);
        // Tests
        mockery.assertIsSatisfied();
        assertNull(output);
    }

    public void testFindBroadCastListContextFromBreakOk() throws Exception {
        // Setup
        final BreakIS<?> input = ISHelper.breakIS_M6_2012();
        final Break breakCore = CoreHelper.break2012M6();
        final BroadcastContextIS contextCommercialIS = ISHelper.broadcastContextCommercial2012M6();
        final BroadcastContext contextCommercialCore = CoreHelper.broadcastContextCommercial2012M6();
        final BroadcastContextIS contextAntenneIS = ISHelper.broadcastContextAntenne2012M6();
        final BroadcastContext contextAntenneCore = CoreHelper.broadcastContextAntenne2012M6();
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Break.class);
                will(returnValue(breakCore));

                // Recherche contexte commercial
                one(serviceMock).findBroadcastFromBreak(breakCore, BroadcastContextCommercial.class);
                will(returnValue(contextCommercialCore));

                one(mapperMock).map(contextCommercialCore, BroadcastContextCommercialIS.class);
                will(returnValue(contextCommercialIS));

                // Recherche contexte antenne
                one(serviceMock).findBroadcastFromBreak(breakCore, BroadcastContextAntenne.class);
                will(returnValue(contextAntenneCore));

                one(mapperMock).map(contextAntenneCore, BroadcastContextAntenneIS.class);
                will(returnValue(contextAntenneIS));

            }
        });
        // Action
        List<BroadcastContextIS> output = rpc.findBroadcastContextListFromBreak(input);
        // Tests
        mockery.assertIsSatisfied();
        Assertions.assertThat(output).isNotNull().hasSize(2).contains(contextCommercialIS, contextAntenneIS);
    }

    public void testFindBroadCastListContextFromBreakOkButCommercialIsNull() throws Exception {
        // Setup
        final BreakIS<?> input = ISHelper.breakIS_M6_2012();
        final Break breakCore = CoreHelper.break2012M6();
        final BroadcastContextIS contextAntenneIS = ISHelper.broadcastContextAntenne2012M6();
        final BroadcastContext contextAntenneCore = CoreHelper.broadcastContextAntenne2012M6();

        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Break.class);
                will(returnValue(breakCore));

                // Recherche contexte commercial
                one(serviceMock).findBroadcastFromBreak(breakCore, BroadcastContextCommercial.class);
                will(returnValue(null));

                // Recherche contexte antenne
                one(serviceMock).findBroadcastFromBreak(breakCore, BroadcastContextAntenne.class);
                will(returnValue(contextAntenneCore));

                one(mapperMock).map(contextAntenneCore, BroadcastContextAntenneIS.class);
                will(returnValue(contextAntenneIS));

            }
        });
        // Action
        List<BroadcastContextIS> output = rpc.findBroadcastContextListFromBreak(input);
        // Tests
        mockery.assertIsSatisfied();
        Assertions.assertThat(output).isNotNull().hasSize(1).contains(contextAntenneIS);
    }

    public void testFindBroadCastListContextFromBreakOkButAntenneIsNull() throws Exception {
        // Setup
        final BreakIS<?> input = ISHelper.breakIS_M6_2012();
        final Break breakCore = CoreHelper.break2012M6();
        final BroadcastContextIS contextCommercialIS = ISHelper.broadcastContextCommercial2012M6();
        final BroadcastContext contextCommercialCore = CoreHelper.broadcastContextCommercial2012M6();

        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Break.class);
                will(returnValue(breakCore));

                // Recherche contexte commercial
                one(serviceMock).findBroadcastFromBreak(breakCore, BroadcastContextCommercial.class);
                will(returnValue(contextCommercialCore));

                one(mapperMock).map(contextCommercialCore, BroadcastContextCommercialIS.class);
                will(returnValue(contextCommercialIS));

                // Recherche contexte antenne
                one(serviceMock).findBroadcastFromBreak(breakCore, BroadcastContextAntenne.class);
                will(returnValue(null));
            }
        });
        // Action
        List<BroadcastContextIS> output = rpc.findBroadcastContextListFromBreak(input);
        // Tests
        mockery.assertIsSatisfied();
        Assertions.assertThat(output).isNotNull().hasSize(1).contains(contextCommercialIS);
    }

    public void testSaveBroadcastContext() throws Exception {
        // Setup
        final BroadcastContextIS inputIS = ISHelper.broadcastContextCommercial2012M6();
        final BroadcastContext inputCore = CoreHelper.broadcastContextCommercial2012M6();
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(inputIS.getTypeContext(), BroadcastContextTypeEnum.class);
                will(returnValue(BroadcastContextTypeEnum.COMMERCIAL));

                one(mapperMock).map(inputIS, BroadcastContextCommercial.class);
                will(returnValue(inputCore));
                inSequence(sequence);

                one(serviceMock).saveOrUpdateContext(inputCore);
                will(returnValue(inputCore));
                inSequence(sequence);

                one(mapperMock).map(inputCore, BroadcastContextCommercialIS.class);
                will(returnValue(inputIS));
                inSequence(sequence);

            }
        });
        // Action
        BroadcastContextIS output = rpc.saveOrUpdateContext(inputIS);
        // Tests
        assertNotNull(output);
        mockery.assertIsSatisfied();
    }

    public void testDeleteContext() throws Exception {
        // Setup
        final BroadcastContextIS inputIS = ISHelper.broadcastContextCommercial2012M6();
        final BroadcastContext inputCore = CoreHelper.broadcastContextCommercial2012M6();
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(inputIS.getTypeContext(), BroadcastContextTypeEnum.class);
                will(returnValue(BroadcastContextTypeEnum.COMMERCIAL));

                one(mapperMock).map(inputIS, BroadcastContextCommercial.class);
                will(returnValue(inputCore));
                inSequence(sequence);

                one(serviceMock).deleteContext(inputCore);
                inSequence(sequence);

            }
        });
        // Action
        rpc.deleteContext(inputIS);
        // Tests
        mockery.assertIsSatisfied();
    }

    /**
     * Test l'appel RPC pour la création d'un groupe de diffusion : cas OK
     */
//    public void testCreateBroadcastGroupOk() throws Exception {
//        // Setup
//        final BroadcastGroupIS inputBroadcastGroupIS = ISHelper.broadcastGroupIS();
//        final List<BroadcastIS> inputBroadcastsIS = Arrays.asList(ISHelper.broadcastSerieIS(), ISHelper.broadcastTeleRealIS());
//
//        final BroadcastGroup mappedBroadcastGroup = CoreHelper.broadcastGroup();
//        final List<Broadcast> mappedBroadcasts = Arrays.asList(CoreHelper.broadcastSerie90(), CoreHelper.broadcastTeleReal());
//
//        mockery.checking(new Expectations() {
//            {
//                // Mapping input
//                one(mapperMock).map(inputBroadcastGroupIS, BroadcastGroup.class);
//                will(returnValue(mappedBroadcastGroup));
//
//                for (int i = 0; i < inputBroadcastsIS.size(); i++) {
//                    one(mapperMock).map(inputBroadcastsIS.get(i), Broadcast.class);
//                    will(returnValue(mappedBroadcasts.get(i)));
//                }
//
//                // Appel au service
//                one(serviceMock).createBroadcastGroup(mappedBroadcastGroup, mappedBroadcasts);
//                will(returnValue(mappedBroadcasts));
//
//                // Mapping output
//                for (int i = 0; i < inputBroadcastsIS.size(); i++) {
//                    one(mapperMock).map(mappedBroadcasts.get(i), BroadcastIS.class);
//                    will(returnValue(inputBroadcastsIS.get(i)));
//                }
//            }
//        });
//
//        // Action
//        rpc.createBroadcastGroup(inputBroadcastGroupIS, inputBroadcastsIS);
//
//        // Tests
//        mockery.assertIsSatisfied();
//    }

    /**
     * Test l'appel RPC pour la création d'un groupe de diffusion : cas KO avec exception métier
     */
//    public void testCreateBroadcastGroupException() throws Exception {
//        // Setup
//        final BroadcastGroupIS inputBroadcastGroupIS = ISHelper.broadcastGroupIS();
//        final List<BroadcastIS> inputBroadcastsIS = Arrays.asList(ISHelper.broadcastSerieIS(), ISHelper.broadcastTeleRealIS());
//
//        final BroadcastGroup mappedBroadcastGroup = CoreHelper.broadcastGroup();
//        final List<Broadcast> mappedBroadcasts = Arrays.asList(CoreHelper.broadcastSerie90(), CoreHelper.broadcastTeleReal());
//
//        final BusinessRuleException throwedException = new BusinessRuleException("Toto");
//        final BusinessRuleExceptionIS outputException = new BusinessRuleExceptionIS(throwedException);
//
//        mockery.checking(new Expectations() {
//            {
//                // Mapping input
//                one(mapperMock).map(inputBroadcastGroupIS, BroadcastGroup.class);
//                will(returnValue(mappedBroadcastGroup));
//
//                for (int i = 0; i < inputBroadcastsIS.size(); i++) {
//                    one(mapperMock).map(inputBroadcastsIS.get(i), Broadcast.class);
//                    will(returnValue(mappedBroadcasts.get(i)));
//                }
//
//                // Appel au service
//                one(serviceMock).createBroadcastGroup(mappedBroadcastGroup, mappedBroadcasts);
//                will(throwException(throwedException));
//            }
//        });
//
//        // Action
//        try {
//            rpc.createBroadcastGroup(inputBroadcastGroupIS, inputBroadcastsIS);
//            fail();
//        } catch (BusinessRuleExceptionIS e) {
//            assertEquals(outputException.getMessage(), e.getMessage());
//        }
//
//        // Tests
//        mockery.assertIsSatisfied();
//    }

    /**
     * Test l'appel RPC pour la suppression d'un groupe de diffusion : cas OK
     */
//    public void testDeleteBroadcastGroupOk() throws Exception {
//        // Setup
//        final BroadcastGroupIS inputBroadcastGroupIS = ISHelper.broadcastGroupIS();
//        final List<BroadcastIS> inputBroadcastsIS = Arrays.asList(ISHelper.broadcastSerieIS(), ISHelper.broadcastTeleRealIS());
//
//        final BroadcastGroup mappedBroadcastGroup = CoreHelper.broadcastGroup();
//        final List<Broadcast> mappedBroadcasts = Arrays.asList(CoreHelper.broadcastSerie90(), CoreHelper.broadcastTeleReal());
//
//        mockery.checking(new Expectations() {
//            {
//                // Mapping input
//                one(mapperMock).map(inputBroadcastGroupIS, BroadcastGroup.class);
//                will(returnValue(mappedBroadcastGroup));
//
//                // Appel au service
//                one(serviceMock).deleteBroadcastGroup(mappedBroadcastGroup);
//                will(returnValue(mappedBroadcasts));
//
//                // Mapping output
//                for (int i = 0; i < mappedBroadcasts.size(); i++) {
//                    one(mapperMock).map(mappedBroadcasts.get(i), BroadcastIS.class);
//                    will(returnValue(inputBroadcastsIS.get(i)));
//                }
//            }
//        });
//
//        // Action
//        rpc.deleteBroadcastGroup(inputBroadcastGroupIS);
//
//        // Tests
//        mockery.assertIsSatisfied();
//    }

    /**
     * Test l'appel RPC pour la suppression d'un groupe de diffusion : cas KO avec exception métier
     */
    public void testDeleteBroadcastGroupException() throws Exception {
        // Setup
        final BroadcastGroupIS inputBroadcastGroupIS = ISHelper.broadcastGroupIS();

        final BroadcastGroup mappedBroadcastGroup = CoreHelper.broadcastGroup();

        final BusinessRuleException throwedException = new BusinessRuleException("Toto");
        final BusinessRuleExceptionIS outputException = new BusinessRuleExceptionIS(throwedException);

        mockery.checking(new Expectations() {
            {
                // Mapping input
                one(mapperMock).map(inputBroadcastGroupIS, BroadcastGroup.class);
                will(returnValue(mappedBroadcastGroup));

                // Appel au service
                one(serviceMock).deleteBroadcastGroup(mappedBroadcastGroup);
                will(throwException(throwedException));
            }
        });

        // Action
        try {
            rpc.deleteBroadcastGroup(inputBroadcastGroupIS);
            fail();
        } catch (BusinessRuleExceptionIS e) {
            assertEquals(outputException.getMessage(), e.getMessage());
        }

        // Tests
        mockery.assertIsSatisfied();
    }

    @Test(expected = RuntimeException.class)
    public void getPriceDataAccessThrowsRuntimeException() throws SkuNotFoundException {
        mockingContext.checking(new Expectations() {
            {
                allowing(mockedDependency).getPriceBySku(with(any(String.class)));
                will(throwException(new RuntimeException("Fatal data access exception.")));
            }
        });
        final BigDecimal price = systemUnderTest.getPrice(SKU);
    }
}
