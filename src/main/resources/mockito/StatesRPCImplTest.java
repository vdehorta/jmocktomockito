package m6.peps.ui.server.rpc.states;

import com.google.common.collect.Lists;
import m6.components.client.rpc.exception.BusinessRuleExceptionIS;
import m6.components.client.rpc.exception.RpcCallTechnicalException;
import m6.foundations.exception.BusinessRuleException;
import m6.foundations.mapper.Mapper;
import m6.peps.core.CoreHelper;
import m6.peps.core.states.DatedChannelStatus;
import m6.peps.service.states.StatesService;
import m6.peps.ui.client.model.core.ISHelper;
import m6.peps.ui.client.model.core.states.DatedChannelStatusIS;
import m6.socle.channel.client.model.core.ChannelIS;
import m6.socle.channel.core.Channel;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * The Class StatesRPCImplTest.
 */
public class StatesRPCImplTest {

    private StatesRPCImpl rpc;

    /**
     * The states service mock.
     */
    private StatesService statesServiceMock = mock(StatesService.class);

    /**
     * The mapper mock.
     */
    private Mapper mapperMock = mock(Mapper.class);

    @BeforeClass
    public static void setupClass() {
    }

    @Before
    public void setUp() throws Exception {
        rpc = new StatesRPCImpl();
        rpc.setStatesService(statesServiceMock);
        rpc.setMapper(mapperMock);
    }


    /**
     * Test get status ok.
     *
     * @throws Exception the exception
     */
    @Test
    public void testGetStatusOK() throws Exception {

        final List<ChannelIS> channels = Lists.newArrayList(ISHelper.channelM6());
        final List<Channel> channelsCore = Lists.newArrayList(CoreHelper.channelM6());
        final List<DatedChannelStatus> datedChannelStatusList = Lists.newArrayList(CoreHelper.datedChannelStatus(new LocalDate(2010, 1, 1), CoreHelper.channelM6(), CoreHelper.status(220, "test")));

        final DatedChannelStatusIS statusIS = ISHelper.datedChannelStatus(new LocalDate(2010, 1, 1), ISHelper.channelM6(), ISHelper.status(220, "test"));

        when(mapperMock.mapList(Lists.newArrayList(ISHelper.channelM6()), Channel.class)).thenReturn(channelsCore);
        when(statesServiceMock.getStatus(channelsCore, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3))).thenReturn(datedChannelStatusList);
        when(mapperMock.mapList(datedChannelStatusList, DatedChannelStatusIS.class)).thenReturn(Lists.newArrayList(statusIS));

        final List<DatedChannelStatusIS> results = rpc.getStatus(channels, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3));

        verify(mapperMock, times(1)).mapList(Lists.newArrayList(ISHelper.channelM6()), Channel.class);
        verify(statesServiceMock, times(1)).getStatus(channelsCore, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3));
        verify(mapperMock, times(1)).mapList(datedChannelStatusList, DatedChannelStatusIS.class);
        assertThat(results).hasSize(1);
    }

    /**
     * Test get status ko.
     *
     * @throws Exception the exception
     */
    @Test(expected = BusinessRuleExceptionIS.class)
    public void testGetStatusKO() throws Exception {

        final List<ChannelIS> channels = Lists.newArrayList(ISHelper.channelM6());
        final List<Channel> channelsCore = Lists.newArrayList(CoreHelper.channelM6());

        when(mapperMock.mapList(Lists.newArrayList(ISHelper.channelM6()), Channel.class)).thenReturn(Lists.newArrayList(CoreHelper.channelM6()));
        when(statesServiceMock.getStatus(channelsCore, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3))).thenThrow(new BusinessRuleException("Exception pendant l'execution"));

        rpc.getStatus(channels, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3));

        verify(mapperMock, times(1)).mapList(Lists.newArrayList(ISHelper.channelM6()), Channel.class);
        verify(statesServiceMock, times(1)).getStatus(channelsCore, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3));
    }

    /**
     * Test get status k o_ rpc call technical exception.
     *
     * @throws Exception the exception
     */
    @Test(expected = RpcCallTechnicalException.class)
    public void testGetStatusKO_RpcCallTechnicalException() throws Exception {

        final List<ChannelIS> channels = Lists.newArrayList(ISHelper.channelM6());
        final List<Channel> channelsCore = Lists.newArrayList(CoreHelper.channelM6());
        final List<DatedChannelStatus> datedChannelStatusList = Lists.newArrayList(CoreHelper.datedChannelStatus(new LocalDate(2010, 1, 1), CoreHelper.channelM6(), CoreHelper.status(220, "test")));

        when(mapperMock.mapList(channels, Channel.class)).thenReturn(channelsCore);
        when(statesServiceMock.getStatus(channelsCore, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3))).thenReturn(datedChannelStatusList);
        when(mapperMock.mapList(datedChannelStatusList, DatedChannelStatusIS.class)).thenThrow(new RpcCallTechnicalException(""));

        rpc.getStatus(channels, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3));

        // Test
        verify(mapperMock, times(1)).mapList(channels, Channel.class);
        verify(statesServiceMock, times(1)).getStatus(channelsCore, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3));
        verify(mapperMock, times(1)).mapList(datedChannelStatusList, DatedChannelStatusIS.class);
    }

    /**
     * Test get status k o_ rpc call technical exception2.
     *
     * @throws Exception the exception
     */
    @Test(expected = RpcCallTechnicalException.class)
    public void testGetStatusKO_RpcCallTechnicalException2() throws Exception {

        final List<ChannelIS> channels = Lists.newArrayList(ISHelper.channelM6());

        final List<DatedChannelStatus> datedChannelStatusList = new ArrayList<DatedChannelStatus>();
        final DatedChannelStatus statusCode = CoreHelper.datedChannelStatus(new LocalDate(2010, 1, 1), CoreHelper.channelM6(), CoreHelper.status(220, "test"));
        datedChannelStatusList.add(statusCode);

        when(mapperMock.mapList(channels, Channel.class)).thenThrow(new RpcCallTechnicalException(""));

        rpc.getStatus(channels, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3));

        // Test
        verify(mapperMock, times(1)).mapList(channels, Channel.class);
    }

    @Test(expected = RuntimeException.class)
    public void testFindAssignedSpotsGetCampaignRuntimeError() throws Exception {

        final CampaignID campaignID = new CampaignID("321321-32");
        final LocalDate beginDate = new LocalDate(2011, 1, 1);
        final LocalDate endDate = new LocalDate(2011, 12, 1);

        when(campaignDaoMock.getByCampaignId(campaignID.getOrderId(), campaignID.getSubOrderId())).thenThrow(new RuntimeException());

        service.findAssignedSpots(campaignID, beginDate, endDate);

        verify(campaignDaoMock, times(1)).getByCampaignId(campaignID.getOrderId(), campaignID.getSubOrderId());
    }
}
