package service.impl;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.Statement;
import org.junit.Test;
import testutils.AbstractTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by vdehorta on 28/03/2017.
 */
public class CodeCheckerServiceImplTest extends AbstractTest {

    private CodeCheckerServiceImpl codeCheckerService = new CodeCheckerServiceImpl();

    @Test
    public void isMockReturnStatement_shouldReturnFalseForInvalidMockReturnStatements() {
        final List<ExpressionStmt> statements = getCompilationUnitOnFile("JMockMockReturnStmt.java").getChildNodesByType(ExpressionStmt.class);

        assertThat(codeCheckerService.isMockReturnStatement(statements.get(5))).isFalse();
        assertThat(codeCheckerService.isMockReturnStatement(statements.get(6))).isFalse();
        assertThat(codeCheckerService.isMockReturnStatement(statements.get(7))).isFalse();
        assertThat(codeCheckerService.isMockReturnStatement(statements.get(8))).isFalse();
        assertThat(codeCheckerService.isMockReturnStatement(statements.get(9))).isFalse();
    }

    @Test
    public void isMockReturnStatement_shouldReturnTrueForValidMockReturnStatements() {
        final List<ExpressionStmt> statements = getCompilationUnitOnFile("JMockMockReturnStmt.java").getChildNodesByType(ExpressionStmt.class);

        assertThat(codeCheckerService.isMockReturnStatement(statements.get(0))).isTrue();
        assertThat(codeCheckerService.isMockReturnStatement(statements.get(1))).isTrue();
        assertThat(codeCheckerService.isMockReturnStatement(statements.get(2))).isTrue();
        assertThat(codeCheckerService.isMockReturnStatement(statements.get(3))).isTrue();
        assertThat(codeCheckerService.isMockReturnStatement(statements.get(4))).isTrue();
    }

    @Test
    public void isMethodCallMock_shouldReturnTrueForValidMethodCallMocks() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockMockCall.java").getChildNodesByType(MethodCallExpr.class);

        MethodCallExpr validMethodCallMock1 = methodCallExprs.get(1);
        MethodCallExpr validMethodCallMock2 = methodCallExprs.get(4);
        MethodCallExpr validMethodCallMock3 = methodCallExprs.get(7);

        assertThat(codeCheckerService.isJMockCallCountMethod(validMethodCallMock1)).isTrue();
        assertThat(codeCheckerService.isJMockCallCountMethod(validMethodCallMock2)).isTrue();
        assertThat(codeCheckerService.isJMockCallCountMethod(validMethodCallMock3)).isTrue();
    }

    @Test
    public void isMethodCallMock_shouldReturnFalseForInvalidMethodCallMocks() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("VariousMethodCall.java").getChildNodesByType(MethodCallExpr.class);
        MethodCallExpr invalidMethodCallMock1 = methodCallExprs.get(0);
        MethodCallExpr invalidMethodCallMock2 = methodCallExprs.get(1);

        assertThat(codeCheckerService.isJMockCallCountMethod(invalidMethodCallMock1)).isFalse();
        assertThat(codeCheckerService.isJMockCallCountMethod(invalidMethodCallMock2)).isFalse();
    }

    @Test
    public void isSequence_ShouldReturnTrueForValidSequences() {
        Statement validSequenceStmt = getCompilationUnitOnFile("JMockSequence.java").getChildNodesByType(Statement.class).get(1);
        assertThat(codeCheckerService.isSequence(validSequenceStmt)).isTrue();
    }

    @Test
    public void isSequence_ShouldReturnFalseForAInvalidSequences() {
        Statement invalidSequenceStmt = getCompilationUnitOnFile("JMockSequence.java").getChildNodesByType(Statement.class).get(2);
        assertThat(codeCheckerService.isSequence(invalidSequenceStmt)).isFalse();
    }

    @Test
    public void isValidJMockMethodCallMockBlock_shouldReturnTrueWhenBlockIsValid() {

        //Given
        final List<ExpressionStmt> statements = getCompilationUnitOnFile("JMockMockery.java").getChildNodesByType(ExpressionStmt.class);
        final ExpressionStmt validJMockExpectationStmt1 = statements.get(0);
        final ExpressionStmt validJMockExpectationStmt2 = statements.get(1);

        //When-Then
        assertThat(codeCheckerService.isJMockExpectationStmt(validJMockExpectationStmt1)).isTrue();
        assertThat(codeCheckerService.isJMockExpectationStmt(validJMockExpectationStmt2)).isTrue();
    }

    @Test
    public void isValidJMockExpectationStmt_shouldReturnFalseWhenMockeryMethodCallIsAbsent() {

        //Given
        final List<ExpressionStmt> statements = getCompilationUnitOnFile("JMockMockery.java").getChildNodesByType(ExpressionStmt.class);
        final ExpressionStmt invalidJMockExpectationStmt = statements.get(3);

        //When-Then
        assertThat(codeCheckerService.isJMockExpectationStmt(invalidJMockExpectationStmt)).isFalse();
    }

    @Test
    public void isValidJMockExpectationStmt_shouldReturnFalseWhenExpectationsParameterIsAbsent() {

        //Given
        final List<ExpressionStmt> statements = getCompilationUnitOnFile("JMockMockery.java").getChildNodesByType(ExpressionStmt.class);
        final ExpressionStmt invalidJMockExpectationStmt = statements.get(3);

        //When-Then
        assertThat(codeCheckerService.isJMockExpectationStmt(invalidJMockExpectationStmt)).isFalse();
    }

    @Test
    public void containsLoop_shouldReturnTrueWhenForLoopIsPresent() {
        final MethodCallExpr methodCallExpr = getCompilationUnitOnFile("ForLoop.java").getChildNodesByType(MethodCallExpr.class).get(0);
        assertThat(codeCheckerService.containsLoop(methodCallExpr)).isTrue();
    }

    @Test
    public void containsLoop_shouldReturnTrueWhenDoLoopIsPresent() {
        final MethodCallExpr methodCallExpr = getCompilationUnitOnFile("DoLoop.java").getChildNodesByType(MethodCallExpr.class).get(0);
        assertThat(codeCheckerService.containsLoop(methodCallExpr)).isTrue();
    }

    @Test
    public void containsLoop_shouldReturnTrueWhenWhileLoopIsPresent() {
        final MethodCallExpr methodCallExpr = getCompilationUnitOnFile("WhileLoop.java").getChildNodesByType(MethodCallExpr.class).get(0);
        assertThat(codeCheckerService.containsLoop(methodCallExpr)).isTrue();
    }

    @Test
    public void containsLoop_shouldReturnFalseWhenNoLoopPresence() {
        final MethodCallExpr methodCallExpr = getCompilationUnitOnFile("JMockMockery.java").getChildNodesByType(MethodCallExpr.class).get(0);
        assertThat(codeCheckerService.containsLoop(methodCallExpr)).isFalse();
    }

    @Test
    public void isJMockAssertIsSatisfiedStmt() {
        final Statement statement = getCompilationUnitOnFile("JMockMethod.java").getChildNodesByType(Statement.class).get(16);
        assertThat(codeCheckerService.isJMockAssertIsSatisfiedStmt(statement)).isTrue();
    }
}