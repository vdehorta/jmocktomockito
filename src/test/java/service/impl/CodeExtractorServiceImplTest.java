package service.impl;

import com.github.javaparser.ast.stmt.Statement;
import org.junit.Test;
import service.ParserService;
import testutils.AbstractTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

public class CodeExtractorServiceImplTest extends AbstractTest {

    private CodeExtractorServiceImpl codeExtractorService = new CodeExtractorServiceImpl();

    @Test
    public void getNextStatement() {
        final List<Statement> statements = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(Statement.class);

        //Given a statement
        final Statement firstStatement = statements.get(2);

        //When
        final Optional<Statement> nextStatementOptional = codeExtractorService.getNextStatement(firstStatement);

        //Then
        assertThat(nextStatementOptional).isPresent();
        assertThat(nextStatementOptional.get()).isEqualTo(statements.get(3));
    }
}