package service.impl;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.Statement;
import org.junit.Test;
import testutils.AbstractTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CodeProducerServiceImplTest extends AbstractTest {

    private CodeProducerServiceImpl codeProducerService = new CodeProducerServiceImpl();

    @Test
    public void insertStmtsAtMethodEnd() {
        //G
        final MethodDeclaration method = getCompilationUnitOnFile("JMockMethod.java").getChildNodesByType(MethodDeclaration.class).get(0);
        final List<Statement> statements = getCompilationUnitOnFile("MockitoVerifynStmts.java").getChildNodesByType(Statement.class);

        //W
        MethodDeclaration methodWithInsertedStmts = codeProducerService.insertStmtsAtMethodEnd(statements, method);

        //T
        //taille initiale 7 + 4 statements dans MockitoVerifynStmts.java
        assertThat(methodWithInsertedStmts.getBody().get().getStatements()).hasSize(11);
        assertThat(methodWithInsertedStmts.getBody().get().getStatement(7)).isEqualTo(statements.get(0));
        assertThat(methodWithInsertedStmts.getBody().get().getStatement(8)).isEqualTo(statements.get(1));
        assertThat(methodWithInsertedStmts.getBody().get().getStatement(9)).isEqualTo(statements.get(2));
        assertThat(methodWithInsertedStmts.getBody().get().getStatement(9)).isEqualTo(statements.get(2));
    }
}