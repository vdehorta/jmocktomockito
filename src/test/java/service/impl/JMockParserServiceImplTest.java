package service.impl;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.IntegerLiteralExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.Statement;
import model.CallCountType;
import model.MockUsage;
import org.junit.Test;
import service.CodeExtractorService;
import service.ServiceFactory;
import testutils.AbstractTest;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static model.MethodCallResultType.VALUE;
import static model.MethodCallResultType.VOID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;


public class JMockParserServiceImplTest extends AbstractTest {

    private JMockParserServiceImpl jMockParserService = new JMockParserServiceImpl();
    private CodeExtractorService codeExtractorService = ServiceFactory.getCodeExtractorService();

    @Test
    public void extractMethodWithoutTestCode_shouldRemoveExpectationStmt() {
        //G
        final MethodDeclaration methodWithExpectationStmt = getCompilationUnitOnFile("JMockMethod.java")
                .getChildNodesByType(MethodDeclaration.class).get(0);
        final Statement expectationStmt = methodWithExpectationStmt.getBody().get().getStatement(3);

        //W
        final MethodDeclaration result = jMockParserService.extractMethodWithoutTestCode(methodWithExpectationStmt);

        //T
        assertThat(result.getBody().get().getStatements()).doesNotContain(expectationStmt);
    }

    @Test
    public void extractMethodWithoutTestCode_shouldRemoveAssertIsSatisfiedStmt() {
        //G
        final MethodDeclaration methodWithExpectationStmt = getCompilationUnitOnFile("JMockMethod.java")
                .getChildNodesByType(MethodDeclaration.class).stream()
                .filter(method -> method.getName().getIdentifier().equals("jMockMethod"))
                .collect(Collectors.toList()).get(0);
        final Statement assertIsSatisfiedStmt = methodWithExpectationStmt.getBody().get().getStatement(6);

        //W
        final MethodDeclaration result = jMockParserService.extractMethodWithoutTestCode(methodWithExpectationStmt);

        //T
        assertThat(result.getBody().get().getStatements()).doesNotContain(assertIsSatisfiedStmt);
    }

    @Test
    public void parseMockUsages() {
        //G
        final MethodDeclaration method = codeExtractorService.extractFirstMatchingNodeOrNull(
                getCompilationUnitOnFile("JMockMethod.java"), MethodDeclaration.class,
                m -> m.getName().getIdentifier().equals("jMockMethod2"));

        //W
        final List<MockUsage> mockUsages = jMockParserService.parseMockUsages(method);

        //T
        assertThat(mockUsages).hasSize(3);
        //mock name
        assertThat(mockUsages)
                .extracting(mockUsage -> mockUsage.getMockName().getName().getIdentifier())
                .containsOnly("mapperMock", "serviceMock", "serviceMock");
        //behaviour simulation
        assertThat(mockUsages).extracting(
                mockUsage -> mockUsage.getBehaviourSimulation().getMethod().getName().getIdentifier(),
                mockUsage -> mockUsage.getBehaviourSimulation().getResultType(),
                mockUsage -> mockUsage.getBehaviourSimulation().getResult(),
                mockUsage -> mockUsage.getBehaviourSimulation().getComments()
        ).containsOnly(
                tuple("map", VALUE, new NameExpr("broadcast"), emptyList()),
                tuple("saveOrUpdate", VALUE, new NameExpr("broadcast"), emptyList()),
                tuple("delete", VOID, null, emptyList()));
        //interaction check
        assertThat(mockUsages).extracting(
                mockUsage -> mockUsage.getInteractionCheck().getMethod().getName().getIdentifier(),
                mockUsage -> mockUsage.getInteractionCheck().getCallCount().getCallCountType(),
                mockUsage -> mockUsage.getInteractionCheck().getCallCount().hasCardinality1(),
                mockUsage -> mockUsage.getInteractionCheck().getCallCount().getCardinality1(),
                mockUsage -> mockUsage.getInteractionCheck().getCallCount().hasCardinality2(),
                mockUsage -> mockUsage.getInteractionCheck().getCallCount().getCardinality2(),
                mockUsage -> mockUsage.getInteractionCheck().getComments()
        ).containsOnly(
                tuple("map", CallCountType.EXACTLY, true, new IntegerLiteralExpr(1), false, null, emptyList()),
                tuple("saveOrUpdate", CallCountType.EXACTLY, true, new IntegerLiteralExpr(1), false, null, emptyList()),
                tuple("delete", CallCountType.EXACTLY, true, new IntegerLiteralExpr(1), false, null, emptyList())
        );
    }

    @Test
    public void parseTestedMethodCallStmt() {
        //G
        final MethodDeclaration method = codeExtractorService.extractFirstMatchingNodeOrNull(
                getCompilationUnitOnFile("JMockMethod.java"), MethodDeclaration.class,
                m -> m.getName().getIdentifier().equals("jMockMethod2"));
        final Statement expectedStmt = codeExtractorService.extractNodes(method, ExpressionStmt.class).get(12);

        //W
        final Statement testedMethodCallStmt = jMockParserService.parseTestedMethodCallStmt(method);

        //T
        assertThat(testedMethodCallStmt).isEqualTo(expectedStmt);

    }
}