package service.impl;

import com.github.javaparser.ast.expr.IntegerLiteralExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.stmt.Statement;
import exception.UnexistingTranslationException;
import model.BehaviourSimulation;
import model.BehaviourSimulation.BehaviourSimulationBuilder;
import model.InteractionCheck;
import model.InteractionCheck.InteractionCheckBuilder;
import model.MethodCallCount;
import model.MethodCallCount.MethodCallCountBuilder;
import model.MethodCallResultType;
import org.junit.Test;
import testutils.AbstractTest;

import java.util.List;

import static model.CallCountType.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by vdehorta on 15/04/2017.
 */
public class MockitoTranslatorServiceImplTest extends AbstractTest {

    private MockitoTranslatorServiceImpl mockitoTranslatorService = new MockitoTranslatorServiceImpl();

    @Test
    public void buildMockitoCallCountMethod_shouldHandleExactlyCallCountType() {

        //G
        final MethodCallCount methodCallCount = new MethodCallCountBuilder()
                .withCallCountType(EXACTLY)
                .withCardinality1(new IntegerLiteralExpr(3))
                .build();

        //W
        MethodCallExpr methodCallExpr = mockitoTranslatorService.toCallCountMethod(methodCallCount);

        //T
        assertThat(methodCallExpr.getName().getId()).isEqualTo("times");
        assertThat(methodCallExpr.getArguments()).hasSize(1);
        assertThat(methodCallExpr.getArguments().get(0)).isEqualTo(new IntegerLiteralExpr(3));
    }

    @Test
    public void buildMockitoCallCountMethod_shouldHandleOneCallCountType() {

        //G
        final MethodCallCount methodCallCount = new MethodCallCountBuilder().withCallCountType(ONE).build();

        //W
        MethodCallExpr methodCallExpr = mockitoTranslatorService.toCallCountMethod(methodCallCount);

        //T
        assertThat(methodCallExpr.getName().getId()).isEqualTo("times");
        assertThat(methodCallExpr.getArguments()).hasSize(1);
        assertThat(methodCallExpr.getArguments().get(0)).isEqualTo(new IntegerLiteralExpr(1));
    }

    @Test
    public void buildMockitoCallCountMethod_shouldHandleAtLeastCallCountType() {

        //G
        final MethodCallCount methodCallCount = new MethodCallCountBuilder()
                .withCallCountType(AT_LEAST)
                .withCardinality1(new IntegerLiteralExpr(2))
                .build();

        //W
        MethodCallExpr methodCallExpr = mockitoTranslatorService.toCallCountMethod(methodCallCount);

        //T
        assertThat(methodCallExpr.getName().getId()).isEqualTo("atLeast");
        assertThat(methodCallExpr.getArguments()).hasSize(1);
        assertThat(methodCallExpr.getArguments().get(0)).isEqualTo(new IntegerLiteralExpr(2));
    }

    @Test
    public void buildMockitoCallCountMethod_shouldHandleAtMostCallCountType() {

        //G
        final MethodCallCount methodCallCount = new MethodCallCountBuilder()
                .withCallCountType(AT_MOST)
                .withCardinality1(new IntegerLiteralExpr(1))
                .build();

        //W
        MethodCallExpr methodCallExpr = mockitoTranslatorService.toCallCountMethod(methodCallCount);

        //T
        assertThat(methodCallExpr.getName().getId()).isEqualTo("atMost");
        assertThat(methodCallExpr.getArguments()).hasSize(1);
        assertThat(methodCallExpr.getArguments().get(0)).isEqualTo(new IntegerLiteralExpr(1));
    }

    @Test(expected = UnexistingTranslationException.class)
    public void buildMockitoCallCountMethod_shouldThrowExceptionForBetweenCallCountType() {

        //G
        final MethodCallCount methodCallCount = new MethodCallCountBuilder()
                .withCallCountType(BETWEEN)
                .withCardinality1(new IntegerLiteralExpr(5))
                .withCardinality2(new IntegerLiteralExpr(7))
                .build();

        //W
        mockitoTranslatorService.toCallCountMethod(methodCallCount);
    }

    /**
     * Tested method : verify(myMock, atLeast(2)).action(1, "2");
     */
    @Test
    public void buildMockitoVerifyStmt_shouldHandleAtLeastCallCountType() {
        final List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockMockCall.java").getChildNodesByType(MethodCallExpr.class);
        final MethodCallExpr mockCall = methodCallExprs.get(5);

        //Given
        final NameExpr mockName = new NameExpr("myMock");
        final InteractionCheck interactionCheck = new InteractionCheckBuilder()
                .withMethod(mockCall)
                .withCallCount(new MethodCallCountBuilder()
                        .withCallCountType(AT_LEAST)
                        .withCardinality1(new IntegerLiteralExpr(2))
                        .build())
                .build();

        Statement verifyMockitoStmt = mockitoTranslatorService.toInteractionCheckStatement(mockName, interactionCheck);

        assertThat(verifyMockitoStmt.toString()).isEqualTo("verify(myMock, atLeast(2)).action(1, \"2\");");
    }

    /**
     * Tested method example : verify(myMock, never()).action(1, "2");
     */
    @Test
    public void buildMockitoVerifyStmt_shouldHandleNeverCallCountType() {
        final List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockMockCall.java").getChildNodesByType(MethodCallExpr.class);
        final MethodCallExpr mockCall = methodCallExprs.get(5);

        //Given
        final NameExpr mockName = new NameExpr("myMock");
        final InteractionCheck callMockCheck = new InteractionCheckBuilder()
                .withMethod(mockCall)
                .withCallCount(new MethodCallCountBuilder()
                        .withCallCountType(NEVER)
                        .build())
                .build();

        Statement verifyMockitoStmt = mockitoTranslatorService.toInteractionCheckStatement(mockName, callMockCheck);

        assertThat(verifyMockitoStmt.toString()).isEqualTo("verify(myMock, never()).action(1, \"2\");");
    }

    /**
     * Tested method example : verify(myMock, never()).action(1, "2");
     */
    @Test
    public void buildMockitoVerifyStmt_shouldHandleJavaExpressionAsCallCountCardinalities() {
        final List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockMockCall.java").getChildNodesByType(MethodCallExpr.class);
        final MethodCallExpr mockCall = methodCallExprs.get(5);
        final MethodCallExpr javaExpressionCardinality = methodCallExprs.get(8);

        //Given
        final NameExpr mockName = new NameExpr("myMock");
        final InteractionCheck callMockCheck = new InteractionCheckBuilder()
                .withMethod(mockCall)
                .withCallCount(new MethodCallCountBuilder()
                        .withCallCountType(AT_MOST)
                        .withCardinality1(javaExpressionCardinality)
                        .build())
                .build();

        Statement verifyMockitoStmt = mockitoTranslatorService.toInteractionCheckStatement(mockName, callMockCheck);

        assertThat(verifyMockitoStmt.toString()).isEqualTo("verify(myMock, atMost(Arrays.asList(1).size())).action(1, \"2\");");
    }

    @Test
    public void buildMockitoMockCall() {
        final List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockMockCall.java").getChildNodesByType(MethodCallExpr.class);

        //Given
        final NameExpr mockName = new NameExpr("newScope");
        final MethodCallExpr mockCall = methodCallExprs.get(5);
        mockCall.setScope(new NameExpr("oldScope_shouldBeOverridenByMockName"));
        final BehaviourSimulation behaviourSimulation = new BehaviourSimulationBuilder()
                .withMethod(mockCall)
                .build();

        //When
        MethodCallExpr mockitoMockCall = mockitoTranslatorService.toBehaviourSimulationMethod(mockName, behaviourSimulation);

        //Then
        assertThat(((NameExpr) mockitoMockCall.getScope().get()).getName().getId()).isNotEqualTo("oldScope_shouldBeOverridenByMockName").isEqualTo("newScope");
        assertThat(mockitoMockCall.getName().getId()).isEqualTo(mockCall.getName().getId());
    }

    @Test
    public void buildWhenThenMockitoStmt() {
        final List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockMockCall.java").getChildNodesByType(MethodCallExpr.class);

        //Given
        final NameExpr mockName = new NameExpr("mock");
        final MethodCallExpr mockCall = methodCallExprs.get(5);
        final BehaviourSimulation behaviourSimulation = new BehaviourSimulationBuilder()
                .withMethod(mockCall)
                .withResultType(MethodCallResultType.VALUE)
                .withResult(new IntegerLiteralExpr(3))
                .build();

        //When
        Statement mockitoWhenThenStmt = mockitoTranslatorService.toWhenThenStmt(mockName, behaviourSimulation);

        //Then
        assertThat(mockitoWhenThenStmt.toString()).isEqualTo("Mockito.when(mock.action(1, \"2\")).thenReturn(3);");
    }
}