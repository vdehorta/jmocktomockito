package service.impl;

import com.github.javaparser.ast.expr.IntegerLiteralExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.SimpleName;
import com.github.javaparser.ast.stmt.Statement;
import model.MethodCallCount;
import org.junit.Test;
import testutils.AbstractTest;

import java.util.List;

import static model.CallCountType.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by vdehorta on 04/04/2017.
 */
public class ParserServiceImplTest extends AbstractTest {

    private JMockParserServiceImpl parserService = new JMockParserServiceImpl();

    @Test
    public void parseMockCallCount_shouldHandleAllowingAndParseItToIgnoringInvocationCount() {
        final List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(0));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(IGNORING);
        assertThat(methodCallCount.getCardinality1()).isNull();
        assertThat(methodCallCount.getCardinality2()).isNull();
    }

    @Test
    public void parseMockCallCount_shouldHandleJavaCodeCardinality() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(9));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(EXACTLY);
        assertThat(methodCallCount.getCardinality1()).isInstanceOf(MethodCallExpr.class);
        final MethodCallExpr parsedCardinalityOne = (MethodCallExpr) methodCallCount.getCardinality1();
        assertThat(parsedCardinalityOne.getScope().get()).isEqualTo(new NameExpr("list"));
        assertThat(parsedCardinalityOne.getName()).isEqualTo(new SimpleName("size"));
    }

    @Test
    public void parseMockCallCount_shouldHandleAtLeastInvocationCount() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(1));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(AT_LEAST);
        assertThat(methodCallCount.getCardinality1()).isEqualTo(new IntegerLiteralExpr(2));
        assertThat(methodCallCount.getCardinality2()).isNull();
    }

    @Test
    public void parseMockCallCount_shouldHandleAtMostInvocationCount() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(2));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(AT_MOST);
        assertThat(methodCallCount.getCardinality1()).isEqualTo(new IntegerLiteralExpr(5));
        assertThat(methodCallCount.getCardinality2()).isNull();
    }

    @Test
    public void parseMockCallCount_shouldHandleBetweenInvocationCount() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(3));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(BETWEEN);
        assertThat(methodCallCount.getCardinality1()).isEqualTo(new IntegerLiteralExpr(1));
        assertThat(methodCallCount.getCardinality2()).isEqualTo(new IntegerLiteralExpr(3));
    }

    @Test
    public void parseMockCallCount_shouldHandleExactlyInvocationCount() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(4));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(EXACTLY);
        assertThat(methodCallCount.getCardinality1()).isEqualTo(new IntegerLiteralExpr(10));
        assertThat(methodCallCount.getCardinality2()).isNull();
    }

    @Test
    public void parseMockCallCount_shouldHandleIgnoringInvocationCount() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(5));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(IGNORING);
        assertThat(methodCallCount.getCardinality1()).isNull();
        assertThat(methodCallCount.getCardinality2()).isNull();
    }

    @Test
    public void parseMockCallCount_shouldHandleNeverInvocationCount() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(6));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(NEVER);
        assertThat(methodCallCount.getCardinality1()).isNull();
        assertThat(methodCallCount.getCardinality2()).isNull();
    }

    @Test
    public void parseMockCallCount_shouldHandleOneAndParseToExactlyOneInvocationCount() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(7));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(EXACTLY);
        assertThat(methodCallCount.getCardinality1()).isEqualTo(new IntegerLiteralExpr(1));
        assertThat(methodCallCount.getCardinality2()).isNull();
    }

    @Test
    public void parseMockCallCount_shouldHandleOneOfAndParseToExactlyOneInvocationCount() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockCallCount.java").getChildNodesByType(MethodCallExpr.class);

        final MethodCallCount methodCallCount = parserService.parseMockCallCount(methodCallExprs.get(8));

        assertThat(methodCallCount).isNotNull();
        assertThat(methodCallCount.getCallCountType()).isEqualTo(EXACTLY);
        assertThat(methodCallCount.getCardinality1()).isEqualTo(new IntegerLiteralExpr(1));
        assertThat(methodCallCount.getCardinality2()).isNull();
    }

    @Test
    public void parseMockName_shouldHandleMocksWithoutOfMethod() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockMockName.java").getChildNodesByType(MethodCallExpr.class);

        final NameExpr nameExpr = parserService.parseMockName(methodCallExprs.get(0), ALLOWING);

        assertThat(nameExpr.getName().getIdentifier()).isEqualTo("turtle1");
    }

    @Test
    public void parseMockName_shouldHandleMocksWithOfMethod() {
        List<MethodCallExpr> methodCallExprs = getCompilationUnitOnFile("JMockMockName.java").getChildNodesByType(MethodCallExpr.class);

        final NameExpr nameExpr = parserService.parseMockName(methodCallExprs.get(2), AT_LEAST);

        assertThat(nameExpr.getName().getIdentifier()).isEqualTo("turtle2");
    }

    @Test
    public void parseMockCall_shouldHandleMocksWithoutOfMethod() {
        List<Statement> statements = getCompilationUnitOnFile("JMockMockCall.java").getChildNodesByType(Statement.class);

        final MethodCallExpr methodCallExpr = parserService.parseMockCall(statements.get(1));

        assertThat(methodCallExpr).isNotNull();
        assertThat(methodCallExpr.getName().getId()).isEqualTo("flashLEDs");
    }

    @Test
    public void parseMockCall_shouldHandleMocksWithOfMethod() {
        List<Statement> statements = getCompilationUnitOnFile("JMockMockCall.java").getChildNodesByType(Statement.class);

        final MethodCallExpr methodCallExpr = parserService.parseMockCall(statements.get(2));

        assertThat(methodCallExpr).isNotNull();
        assertThat(methodCallExpr.getName().getIdentifier()).isEqualTo("stop");
    }

    @Test
    public void parseMockCall_shouldHandleMocksWithArguments() {
        List<Statement> statements = getCompilationUnitOnFile("JMockMockCall.java").getChildNodesByType(Statement.class);

        final MethodCallExpr methodCallExpr = parserService.parseMockCall(statements.get(3));

        assertThat(methodCallExpr).isNotNull();
        assertThat(methodCallExpr.getName().getId()).isEqualTo("action");
        assertThat(methodCallExpr.getArguments()).hasSize(2);
        assertThat(methodCallExpr.getArgument(0).toString()).isEqualTo("1");
        assertThat(methodCallExpr.getArgument(1).toString()).isEqualTo("\"2\"");
    }


}