package service.impl;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.Comment;
import org.junit.Test;
import service.impl.ReformatServiceImpl;
import testutils.AbstractTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by vdehorta on 19/03/2017.
 */
public class ReformatServiceImplTest extends AbstractTest {

    private ReformatServiceImpl reformattingService = new ReformatServiceImpl();

    @Test
    public void fromMultipleLineCommentsToBlockComment() throws Exception {

        //Given
        CompilationUnit compilationUnit = getCompilationUnitOnFile("MultipleLineComments.java");

        //When
        reformattingService.fromMultipleLineCommentsToBlockComment(compilationUnit);

        //Then
        assertThat(compilationUnit.getAllContainedComments()).hasSize(5);
        final Comment transformedComment = compilationUnit.getAllContainedComments().get(4);
        assertThat(transformedComment instanceof BlockComment);
        assertThat(transformedComment.getContent()).contains("in block 1");
        assertThat(transformedComment.getContent()).contains("in block 2");
    }

}