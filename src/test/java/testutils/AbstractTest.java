package testutils;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by vdehorta on 03/10/2016.
 */
public class AbstractTest {

    private static final String PATH_BASE = ".\\src\\test\\resources\\";

    protected CompilationUnit getCompilationUnitOnFile(final String relativeFilePath) {
        FileInputStream in = null;
        CompilationUnit compilationUnit = null;
        try {
            in = new FileInputStream(PATH_BASE + relativeFilePath);
            compilationUnit = JavaParser.parse(in);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
        return compilationUnit;
    }
}
