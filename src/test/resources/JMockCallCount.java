public class MethodCallExpr {

    public void method() {
        allowing(calculator);
        atLeast(2);
        atMost(5);
        between(1, 3);
        exactly(10);
        ignoring(calculator);
        never(calculator);
        one(calculator);
        oneOf(calculator);
        exactly(list.size());
    }
}