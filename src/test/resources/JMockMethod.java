public class MethodCallExpr {

    public void jMockMethod() throws Exception {
        // Setup
        final BroadcastIS input = ISHelper.broadcastSerieIS();

        final Broadcast broadcast = CoreHelper.broadcastSerie90();
        broadcast.getChannel().setId(input.getChannel().getId());

        // Define mock behaviour
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Broadcast.class);
                will(returnValue(broadcast));

                inSequence(sequence);
                one(serviceMock).saveOrUpdate(broadcast);
                will(returnValue(broadcast));
                inSequence(sequence);

                one(mapperMock).map(broadcast, BroadcastIS.class);
                will(returnValue(input));

            }
        });
        // Action
        BroadcastIS output = rpc.saveOrUpdate(input);

        // Test
        assertNotNull(output);
        mockery.assertIsSatisfied();
    }

    public void jMockMethod2() throws Exception {
        // Setup
        final BroadcastIS input = ISHelper.broadcastSerieIS();

        final Broadcast broadcast = CoreHelper.broadcastSerie90();
        broadcast.getChannel().setId(input.getChannel().getId());

        // Define mock behaviour
        mockery.checking(new Expectations() {
            {
                one(mapperMock).map(input, Broadcast.class);
                will(returnValue(broadcast));

                inSequence(sequence);
                one(serviceMock).saveOrUpdate(broadcast);
                will(returnValue(broadcast));
                inSequence(sequence);

                inSequence(sequence);
                one(serviceMock).delete(broadcast);
            }
        });
        // Action
        BroadcastIS output = rpc.saveOrUpdate(input);

        // Test
        assertNotNull(output);
        mockery.assertIsSatisfied();
    }
}
