import java.util.Arrays;

public class MethodCallExpr {

    public void method() {
        allowing(toto).flashLEDs();
        atLeast(1).of(turtle).stop();
        atLeast(1).of(turtle).action(1, "2");
        Arrays.asList(1).size();
    }
}