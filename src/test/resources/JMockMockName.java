public class MethodCallExpr {

    public void method() {
        allowing(turtle1);
        atLeast(1).of(turtle2);
    }
}