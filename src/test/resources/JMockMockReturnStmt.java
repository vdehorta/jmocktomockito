public class MethodCallExpr {

    public void method() {

        will(returnValue(v)); //Valid
        will(returnIterator(c)); //Valid
        will(returnIterator(v1, v2, v3)); //Valid
        will(throwException(e)); //Valid
        will(doAll(a1, a2)); //Valid

        wills(returnValue(v)); //Invalid
        will(); //Invalid
        will(returnValue); //Invalid
        will(wrongReturnMethod(toto)); //Invalid
        will(returnValue()); //Invalid
    }
}