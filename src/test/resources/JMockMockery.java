public class MethodCallExpr {

    public void method() {

        //Valid 1
        mockery.checking(new Expectations() {
            int i = 1;
        });

        //Valid 2
        mockery.checking(new Expectations(){int i = 1;});

        //Invalid 1 : checkings instead of checking
        mockery.checkings(new Expectations() {
            int i = 1;
        });

        //Invalid 2 : Object instead of Expectations
        mockery.checking(new Object() {
            int i = 1;
        });
    }
}
