public class MethodCallExpr {

    public void method() {
        verify(mockObject, times(2)).someMethod("toto");
    }
}