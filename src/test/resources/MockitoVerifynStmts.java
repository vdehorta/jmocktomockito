import com.google.common.collect.Lists;

public class MethodCallExpr {

    public void method() {
        verify(mapperMock, times(1)).mapList(Lists.newArrayList(ISHelper.channelM6()), Channel.class);
        verify(statesServiceMock, times(1)).getStatus(channelsCore, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3));
        verify(mapperMock, times(1)).mapList(datedChannelStatusList, DatedChannelStatusIS.class);
    }
}
