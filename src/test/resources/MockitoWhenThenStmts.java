import com.google.common.collect.Lists;

public class MethodCallExpr {

    public void method() {
        when(mapperMock.mapList(Lists.newArrayList(ISHelper.channelM6()), Channel.class)).thenReturn(channelsCore);
        when(statesServiceMock.getStatus(channelsCore, new LocalDate(2010, 1, 1), new LocalDate(2010, 1, 3))).thenReturn(datedChannelStatusList);
        when(mapperMock.mapList(datedChannelStatusList, DatedChannelStatusIS.class)).thenReturn(Lists.newArrayList(statusIS));
    }
}
