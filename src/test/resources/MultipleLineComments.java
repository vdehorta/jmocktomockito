public class MethodCallExpr {

    //method
    public void method() {

        //block in method
        mockery.checking(new Expectations() {
            {
                //in block 1
                //in block 2
                i = i + 1;
            }
            //bottom of block
        });
    }
    //bottom of method
}
